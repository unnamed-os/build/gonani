package defs

import "regexp"

var RulesFileRegex = regexp.MustCompile("^(.*?[.]|^)build.rules.(ya?ml|json)$")
var DeviceConfigFileRegex = regexp.MustCompile("^(.*?[._-]|^)device.config.(ya?ml|json)$")
var ProviderFileRegex = regexp.MustCompile("^.*?[._-]provider.(ya?ml|json)$")

type RuleIdentifier = string

const FileDependencyPrefix = "//"

package defs

type NativeObjectType string

const (
	NOTypeShared     NativeObjectType = "shared"
	NOTypeStatic     NativeObjectType = "static"
	NOTypeSingle     NativeObjectType = "single"
	NOTypeExecutable NativeObjectType = "executable"
	NOTypeCustom     NativeObjectType = "custom"
	NOTypeRLib       NativeObjectType = "rlib"
)

func (t NativeObjectType) FileExt() string {
	switch t {
	case NOTypeShared:
		return ".so"
	case NOTypeStatic:
		return ".a"
	case NOTypeSingle:
		return ".o"
	case NOTypeExecutable:
		return ""
	case NOTypeRLib:
		return ".rlib"
	case NOTypeCustom:
		fallthrough
	default:
		return ".bin"
	}
}

func (t NativeObjectType) AddExtTo(filename string) string {
	return filename + t.FileExt()
}

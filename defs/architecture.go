package defs

import (
	"fmt"
	"strings"
)

type ArchitectureGroup []Architecture
type Architecture string

type ProjectConfigSubset interface {
	RustChannel() string
}

var ProjectConfiguration ProjectConfigSubset

const (
	// AMD64

	ArchAMD64 Architecture = "amd64"
	ArchX64   Architecture = "x86_64"

	// Intel x86

	ArchX86 Architecture = "x86"

	// ARMv6

	ArchARMv6 Architecture = "armv6"

	// ARMv7

	ArchARMv7 Architecture = "armv7"
	ArchARM   Architecture = "arm"

	// AArch64

	ArchARMv8a  Architecture = "armv8a"
	ArchAArch64 Architecture = "aarch64"
	ArchARM64   Architecture = "arm64"

	// RISC-V

	ArchRISCV32 Architecture = "riscv32"
	ArchRISCV64 Architecture = "riscv64"

	// AVR

	ArchAVR Architecture = "avr"
)

var (
	AMD64   = ArchitectureGroup{ArchX64, ArchAMD64}
	X86     = ArchitectureGroup{ArchX86}
	ARMv6   = ArchitectureGroup{ArchARMv6}
	ARMv7   = ArchitectureGroup{ArchARM, ArchARMv7}
	AArch64 = ArchitectureGroup{ArchARM64, ArchAArch64, ArchARMv8a}
	RISCV32 = ArchitectureGroup{ArchRISCV32}
	RISCV64 = ArchitectureGroup{ArchRISCV64}
	AVR     = ArchitectureGroup{ArchAVR}
)

var allArchitectures = []ArchitectureGroup{
	AMD64, X86, ARMv6, ARMv7, AArch64, RISCV32, RISCV64, AVR,
}

func (a ArchitectureGroup) IsValid() bool {
	for _, arch := range allArchitectures {
		for _, name := range a {
			for _, aName := range arch {
				if aName == name {
					return true
				}
			}
		}
	}
	return false
}

func (a Architecture) String() string {
	return string(a)
}

func IsArchitectureSupported(archName string) bool {
	for _, arch := range allArchitectures {
		for _, name := range arch {
			if strings.EqualFold(archName, name.String()) {
				return true
			}
		}
	}
	return false
}

func ArchitectureGroupForName(archName string) ArchitectureGroup {
	for _, arch := range allArchitectures {
		for _, name := range arch {
			if strings.EqualFold(archName, name.String()) {
				return arch
			}
		}
	}
	panic(fmt.Sprintf("Unsupported architecture %s", archName))
}

func (a Architecture) ArchitectureGroup() ArchitectureGroup {
	return ArchitectureGroupForName(a.String())
}

func (a Architecture) Normalize() Architecture {
	return a.ArchitectureGroup()[0]
}

func (a Architecture) RustTargetTriple() string {
	switch a {
	case ArchX64:
		return "x86_64-unknown-linux-musl"
	case ArchX86:
		return "i686-unknown-linux-gnu"
	default:
		return a.String() + "-unknown-none"
	}
}

package defs

type BuildFor string

const (
	Host     BuildFor = "host"
	Target   BuildFor = "target"
	OSTarget BuildFor = "os_target"
)

func (t BuildFor) IsValid() bool {
	switch t {
	case
		Host,
		Target,
		OSTarget:
		return true
	default:
		return false
	}
}

func (t BuildFor) String() string {
	return string(t)
}

func (t BuildFor) NeedsCrossCompiler() bool {
	return t == Target || t == OSTarget
}

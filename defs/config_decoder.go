package defs

import (
	"github.com/mitchellh/mapstructure"
)

func ConfigDecoder(result interface{}) *mapstructure.Decoder {
	configDecoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		ErrorUnused:      true,
		ZeroFields:       false,
		WeaklyTypedInput: true,
		Squash:           true,
		Result:           result,
		TagName:          "yaml",
	})
	if err != nil {
		panic(err)
	}
	return configDecoder
}

func MustDecodeConfig(dest interface{}, src interface{}) {
	err := ConfigDecoder(dest).Decode(src)
	if err != nil {
		panic(err)
	}
}

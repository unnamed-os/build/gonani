package defs

type TargetType string

const (
	GoBinary     TargetType = "go-binary"
	AsmObject    TargetType = "asm-object"
	NativeBinary TargetType = "native-binary"
	RustBinary   TargetType = "rust-binary"
	Make         TargetType = "make"
	Touch        TargetType = "touch"
	CopyHeaders  TargetType = "copy-headers"
)

func (t TargetType) IsValid() bool {
	switch t {
	case
		GoBinary,
		AsmObject,
		NativeBinary,
		Make,
		Touch:
		return true
	default:
		return false
	}
}

func (t TargetType) String() string {
	return string(t)
}

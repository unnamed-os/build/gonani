package rustup

import (
	"fmt"
	"gonani/cmds"
	"gonani/config/runtime_config"

	"gitlab.com/xdevs23/go-runtimeutil"
)

var ProgramPath string

func InstallRustComponent(component string) {
	if !runtime_config.Config.ProjectConfig().UseRust() {
		fmt.Printf("Note: rust is not enabled. %s is no-op.\n", runtimeutil.ThisFuncName())
		return
	}
	err := cmds.RunCommand(
		ProgramPath, "+"+runtime_config.Config.ProjectConfig().RustChannel(), "component", "add", component,
	)
	if err != nil {
		fmt.Println("Error while installing", component)
		panic("cannot install rust component")
	}
}

func AddTarget(target string) {
	if !runtime_config.Config.ProjectConfig().UseRust() {
		fmt.Printf("Note: rust is not enabled. %s is no-op.\n", runtimeutil.ThisFuncName())
		return
	}
	err := cmds.RunCommand(
		ProgramPath, "+"+runtime_config.Config.ProjectConfig().RustChannel(), "target", "add", target,
	)
	if err != nil {
		fmt.Println("Error while adding", target)
		panic("cannot add rust target")
	}
}

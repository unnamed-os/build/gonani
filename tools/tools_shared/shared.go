package tools_shared

import (
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs/paths"
	"gonani/target/target_utils"
	"path"
	"strings"
)

type HeadersFrom = map[defs.RuleIdentifier][]string

type NativeHeaderTemplateBits struct {
	Arch defs.Architecture
}

func NativeHeaderDirsCmdLineArgs(headerPaths []string) []string {
	args := make([]string, 0, len(headerPaths))
	for _, headerPath := range headerPaths {
		args = append(args, "-I"+headerPath)
	}
	return args
}

func NativeHeaderUsesCmdLineArgs(headerMap map[defs.RuleIdentifier][]string) []string {
	cmdLineArgs := make([]string, 0, len(headerMap))

	for ruleIDFull, innerDirs := range headerMap {
		ruleIDSplit := strings.Split(ruleIDFull, "::!")
		headerDir := paths.OuterHeaderDirOf(ruleIDSplit[0])
		if len(ruleIDSplit) == 2 && ruleIDSplit[1] == "direct" {
			headerDir = paths.HeaderDirOf(ruleIDSplit[0])
		}
		for _, innerDir := range innerDirs {
			finalInnerDir := path.Clean(target_utils.ProcessTemplate(innerDir, NativeHeaderTemplateBits{
				Arch: runtime_config.DeviceArchitecture(),
			}))

			finalHeaderDir := headerDir
			if finalInnerDir != "." {
				finalHeaderDir = paths.HeaderDirOf(ruleIDSplit[0])
			}

			finalInnerDir = path.Clean(path.Join(finalHeaderDir, finalInnerDir))

			cmdLineArgs = append(cmdLineArgs, "-I"+finalInnerDir)
		}
	}

	return cmdLineArgs
}

func NativeCompilerTypicalInOutArguments(sourceFiles []string) []string {
	return append(append([]string{"-c"}, sourceFiles...), "-o", "$out")
}

func NativeLinkerTypicalOutArguments() []string {
	return []string{"-o", "$out"}
}

func NativeObjCopyTypicalInOutArguments() []string {
	return []string{"$in", "$out"}
}

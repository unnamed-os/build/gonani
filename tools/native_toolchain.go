package tools

import (
	"gonani/defs"
	"gonani/shell"
	"gonani/tools/tools_shared"
)

type NativeToolchain interface {
	CCompiler() CCompiler
	CXXCompiler() CXXCompiler
	Assembler() Assembler
	Linker() Linker
	ObjCopy() ObjCopy
}

type NativeToolchainOptions struct {
	// OverrideTarget instructs the compiler etc. to use a specific target
	// instead of the device architecture. Note that not all compilers support this,
	// most notably GCC. If you want to use this option, you're best off using a clang provider.
	OverrideTarget string
	// PrioritizedHeaderSearchPaths are specified first. This should include the source
	// directory of the file being compiled
	PrioritizedHeaderSearchPaths []string
	// SourceFiles specifies files to be compiled
	SourceFiles []string
	// UseCCAsLinker is true when the CC program should be used for linking.
	// Specifying this is vital as the command line arguments are different
	// when the CC is used.
	UseCCAsLinker bool
	// DepFile is used when compiling C/C++ files. Specifying this adds the flags
	// necessary to generate dependency files that ninja will use to do incremental
	// building based on included headers.
	DepFile string
	// BuildFor specifies the target type
	BuildFor defs.BuildFor
}

type NativeToolchainComponentBase struct {
}

type CCompiler interface {
	Arguments(cflags []string, headersFrom tools_shared.HeadersFrom, options NativeToolchainOptions) shell.Arguments
}

type CXXCompiler interface {
	Arguments(cxxFlags []string, headersFrom tools_shared.HeadersFrom, options NativeToolchainOptions) shell.Arguments
}

type Assembler interface {
	Arguments(asFlags []string, headersFrom tools_shared.HeadersFrom, options NativeToolchainOptions) shell.Arguments
}

type Linker interface {
	Arguments(
		ldFlags []string,
		linkType defs.NativeObjectType,
		searchDirs []string,
		linkLibNames []string,
		linkObjects []string,
		linkerFile string,
		options NativeToolchainOptions,
	) shell.Arguments
}

type ObjCopy interface {
	Arguments(args []string, options NativeToolchainOptions) shell.Arguments
}

package toolchains

import (
	"gonani/collections"
	"gonani/defs"
	"gonani/shell"
	"gonani/tools"
	"gonani/tools/tools_shared"
)

type GCCToolchain struct {
	gcc      GCC
	gxx      GXX
	ld       GCCLinker
	as       GCCAssembler
	binutils GNUBinutils
}

func (g *GCCToolchain) CCompiler() tools.CCompiler {
	return &g.gcc
}

func (g *GCCToolchain) CXXCompiler() tools.CXXCompiler {
	return &g.gxx
}

func (g *GCCToolchain) Assembler() tools.Assembler {
	return &g.as
}

func (g *GCCToolchain) Linker() tools.Linker {
	return &g.ld
}

func (g *GCCToolchain) ObjCopy() tools.ObjCopy {
	return g.binutils.ObjCopy()
}

type GCC struct{}

func (GCC) Arguments(
	cflags []string, headersFrom tools_shared.HeadersFrom, options tools.NativeToolchainOptions,
) shell.Arguments {
	return *shell.
		ArgumentsFromStrings(cflags...).
		AppendStringsIf(len(options.DepFile) > 0, "-MD", "-MF", options.DepFile).
		AppendStrings(tools_shared.NativeHeaderDirsCmdLineArgs(options.PrioritizedHeaderSearchPaths)...).
		AppendStrings(tools_shared.NativeHeaderUsesCmdLineArgs(headersFrom)...).
		AppendStrings(tools_shared.NativeCompilerTypicalInOutArguments(options.SourceFiles)...)
}

type GXX struct{}

func (GXX) Arguments(
	cxxFlags []string, headersFrom tools_shared.HeadersFrom, options tools.NativeToolchainOptions,
) shell.Arguments {
	return *shell.
		ArgumentsFromStrings(cxxFlags...).
		AppendStrings(tools_shared.NativeHeaderDirsCmdLineArgs(options.PrioritizedHeaderSearchPaths)...).
		AppendStrings(tools_shared.NativeHeaderUsesCmdLineArgs(headersFrom)...).
		AppendStrings(tools_shared.NativeCompilerTypicalInOutArguments(options.SourceFiles)...)
}

type GCCLinker struct{}

func (GCCLinker) Arguments(
	ldFlags []string,
	linkType defs.NativeObjectType,
	searchDirs []string,
	linkLibNames []string,
	linkObjects []string,
	linkerFile string,
	_ tools.NativeToolchainOptions,
) shell.Arguments {
	args := shell.ArgumentsFromStrings(collections.FilterOutSlice(ldFlags, func(entry interface{}) bool {
		return entry.(string) == "-lgcc"
	}).([]string)...)

	switch linkType {
	case defs.NOTypeShared:
		args.AppendStrings("-shared")
	case defs.NOTypeStatic:
		panic("Static libraries are not implemented yet")
	}

	for _, searchDir := range searchDirs {
		args.AppendStrings("-L" + searchDir)
	}

	for _, linkLibName := range linkLibNames {
		args.AppendStrings("-l:" + linkLibName)
	}

	if len(linkerFile) > 0 {
		args.AppendStrings("-T", linkerFile)
	}

	args.AppendStrings(linkObjects...)

	if collections.HasString(ldFlags, "-lgcc") {
		args.AppendStrings("-lgcc")
	}

	args.AppendStrings(tools_shared.NativeLinkerTypicalOutArguments()...)

	return *args
}

type GCCAssembler struct{}

func (GCCAssembler) Arguments(
	asFlags []string, headersFrom tools_shared.HeadersFrom, options tools.NativeToolchainOptions,
) shell.Arguments {
	return *shell.
		ArgumentsFromStrings(asFlags...).
		AppendStrings(tools_shared.NativeHeaderUsesCmdLineArgs(headersFrom)...).
		AppendStrings(tools_shared.NativeCompilerTypicalInOutArguments(options.SourceFiles)...)
}

type GNUBinutils struct {
	objCopy GNUObjCopy
}

func (b *GNUBinutils) ObjCopy() tools.ObjCopy {
	return b.objCopy
}

type GNUObjCopy struct{}

func (GNUObjCopy) Arguments(args []string, _ tools.NativeToolchainOptions) shell.Arguments {
	return *shell.ArgumentsFromStrings(args...).AppendStrings(tools_shared.NativeObjCopyTypicalInOutArguments()...)
}

var _ tools.NativeToolchain = (*GCCToolchain)(nil)

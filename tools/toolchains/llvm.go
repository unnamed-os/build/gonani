package toolchains

import (
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/shell"
	"gonani/tools"
	"gonani/tools/tools_shared"
)

type LLVMToolchain struct {
	clang    Clang
	ld       LLD
	as       LLVMAssembler
	binutils LLVMBinutils
	Options  LLVMOptions
}

type LLVMOptions struct {
	LDNoTarget            bool `yaml:"ld_no_target"`
	LDUseLLDLinkOutSyntax bool `yaml:"ld_use_lld_link_out_syntax"`
	LLVMAutoDetectVersion bool `yaml:"llvm_autodetect_version"`
	LLVMMinVersion        int  `yaml:"llvm_min_version"`
	LLVMMaxVersion        int  `yaml:"llvm_max_version"`
}

func (l *LLVMToolchain) CCompiler() tools.CCompiler {
	return &l.clang
}

func (l *LLVMToolchain) CXXCompiler() tools.CXXCompiler {
	return &l.clang
}

func (l *LLVMToolchain) Assembler() tools.Assembler {
	return &l.as
}

func (l *LLVMToolchain) Linker() tools.Linker {
	l.ld.options = l.Options
	return &l.ld
}

func (l *LLVMToolchain) ObjCopy() tools.ObjCopy {
	return l.binutils.ObjCopy()
}

type Clang struct{}

func defaultLLVMTarget() string {
	defaultTarget := runtime_config.DeviceArchitecture().String()
	if defaultTarget == defs.ArchX86.String() {
		defaultTarget = "i786"
	}
	return defaultTarget
}

func (Clang) Arguments(
	cflags []string, headersFrom tools_shared.HeadersFrom, options tools.NativeToolchainOptions,
) shell.Arguments {

	return *shell.
		ArgumentsFromStrings(cflags...).
		AppendStringsIf(len(options.DepFile) > 0, "-MD", "-MF", options.DepFile).
		AppendStrings("-target").
		AppendStringsIf(len(options.OverrideTarget) > 0, options.OverrideTarget).
		AppendStringsIf(len(options.OverrideTarget) == 0, defaultLLVMTarget()).
		AppendStrings(tools_shared.NativeHeaderDirsCmdLineArgs(options.PrioritizedHeaderSearchPaths)...).
		AppendStrings(tools_shared.NativeHeaderUsesCmdLineArgs(headersFrom)...).
		AppendStrings(tools_shared.NativeCompilerTypicalInOutArguments(options.SourceFiles)...)
}

type LLD struct {
	options LLVMOptions
}

func (l LLD) Arguments(
	ldFlags []string,
	linkType defs.NativeObjectType,
	searchDirs []string,
	linkLibNames []string,
	linkObjects []string,
	linkerFile string,
	options tools.NativeToolchainOptions,
) shell.Arguments {
	args := &shell.Arguments{}

	if !l.options.LDNoTarget {
		args.AppendStrings("-target").
			AppendStringsIf(len(options.OverrideTarget) > 0, options.OverrideTarget).
			AppendStringsIf(len(options.OverrideTarget) == 0, defaultLLVMTarget())
	}

	args.AppendStrings(ldFlags...)

	switch linkType {
	case defs.NOTypeShared:
		args.AppendStrings("-shared")
	case defs.NOTypeStatic:
		panic("Static libraries are not implemented yet")
	}

	for _, searchDir := range searchDirs {
		args.AppendStrings("-L" + searchDir)
	}

	for _, linkLibName := range linkLibNames {
		args.AppendStrings("-l" + linkLibName)
	}

	if len(linkerFile) > 0 {
		args.AppendStrings("-T", linkerFile)
	}

	args.AppendStrings(linkObjects...)

	if !l.options.LDUseLLDLinkOutSyntax || options.UseCCAsLinker {
		args.AppendStrings(tools_shared.NativeLinkerTypicalOutArguments()...)
	} else {
		args.AppendStrings("-out:$out")
	}

	return *args
}

type LLVMAssembler struct{}

func (LLVMAssembler) Arguments(
	asFlags []string, headersFrom tools_shared.HeadersFrom, options tools.NativeToolchainOptions,
) shell.Arguments {
	return *shell.
		ArgumentsFromStrings(asFlags...).
		AppendStrings("-target").
		AppendStringsIf(len(options.OverrideTarget) > 0, options.OverrideTarget).
		AppendStringsIf(len(options.OverrideTarget) == 0, defaultLLVMTarget()).
		AppendStrings(tools_shared.NativeHeaderDirsCmdLineArgs(options.PrioritizedHeaderSearchPaths)...).
		AppendStrings(tools_shared.NativeHeaderUsesCmdLineArgs(headersFrom)...).
		AppendStrings(tools_shared.NativeCompilerTypicalInOutArguments(options.SourceFiles)...)
}

type LLVMBinutils struct {
	objCopy LLVMObjCopy
}

func (b *LLVMBinutils) ObjCopy() tools.ObjCopy {
	return b.objCopy
}

type LLVMObjCopy struct{}

func (LLVMObjCopy) Arguments(args []string, options tools.NativeToolchainOptions) shell.Arguments {
	return *shell.ArgumentsFromStrings(args...).
		AppendStrings("-target").
		AppendStringsIf(len(options.OverrideTarget) > 0, options.OverrideTarget).
		AppendStringsIf(len(options.OverrideTarget) == 0, defaultLLVMTarget()).
		AppendStrings(tools_shared.NativeObjCopyTypicalInOutArguments()...)
}

var _ tools.NativeToolchain = (*LLVMToolchain)(nil)

package collector

import (
	"fmt"
	"gonani/fs"
	"gonani/rule"
	"path"

	"gopkg.in/yaml.v3"
)

type Collector struct {
	ProjectDir string

	RulesChan chan rule.Rules
}

func (p *Collector) Collect() {
	resultChan := fs.Walk(p.ProjectDir)
	for {
		result, hasMore := <-resultChan
		if !hasMore || result.Content == nil && result.Err == nil {
			break
		}
		if result.Err != nil {
			panic(result.Err)
		}
		rules := rule.Rules{}
		err := yaml.Unmarshal(result.Content, rules)
		if err != nil {
			panic(err)
		}
		fmt.Printf("Build rules collected: %d\n", len(rules))
		for identifier, r := range rules {
			r.Identifier = identifier
			r.RulePath = result.Path
			r.SetRuleDir(path.Dir(result.Path))
		}
		p.RulesChan <- rules
	}
	p.RulesChan <- nil
	close(p.RulesChan)
}

package runtime_config

import (
	"gonani/defs"
	"path/filepath"
)

type ConfigurationInterface interface {
	ProjectDir() string
	OutputDir() string
	ProjectConfig() ProjectConfigurationInterface
	DeviceConfig() DeviceConfig
	EnvConfig() EnvConfigurationInterface
	ReadDeviceConfig(targetDevice string)
}

type ProjectConfigurationInterface interface {
	Targets() []defs.RuleIdentifier
	FillDefaultsAndValidate()
	RustChannel() string
	UseRust() bool
}

type EnvConfigurationInterface interface {
	Assembler(buildFor defs.BuildFor, providerId string) string
	CCompiler(buildFor defs.BuildFor, providerId string) string
	CXXCompiler(buildFor defs.BuildFor, providerId string) string
	Linker(buildFor defs.BuildFor, providerId string) string
	ObjCopy(buildFor defs.BuildFor, providerId string) string
	CFlags(buildFor defs.BuildFor) []string
	CXXFlags(buildFor defs.BuildFor) []string
	LDFlags(buildFor defs.BuildFor) []string
	ASFlags(buildFor defs.BuildFor) []string
	NativeHostCC() string
	NativeHostCXX() string
	NativeHostLD() string
	NativeHostAS() string
	NativeHostCFlags() []string
	NativeHostCXXFlags() []string
	NativeHostLDFlags() []string
	NativeHostASFlags() []string
	GoCommand() string
	MakeCommand() string
	ShellCommand() string
}

type DeviceConfig struct {
	DeviceName           string                `yaml:"device_name"`
	Architecture         defs.Architecture     `yaml:"arch"`
	CrossCompileRuleName defs.RuleIdentifier   `yaml:"cross_toolchain_id"`
	Targets              []defs.RuleIdentifier `yaml:"targets"`
}

var Config ConfigurationInterface

func RelOutputDir() string {
	rel, _ := filepath.Rel(Config.ProjectDir(), Config.OutputDir())
	return rel
}

func ProjectDir() string {
	return Config.ProjectDir()
}

func OutputDir() string {
	return Config.OutputDir()
}

func DeviceArchitecture() defs.Architecture {
	return Config.DeviceConfig().Architecture
}

func DeviceName() string {
	return Config.DeviceConfig().DeviceName
}

func Env() EnvConfigurationInterface {
	return Config.EnvConfig()
}

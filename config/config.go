package config

import (
	"fmt"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"gonani/provider"
	"io"
	"os"
	"path"

	"github.com/goccy/go-yaml"
)

const DefaultConfigFile = "build/config/build.config.yaml"

const EnvVarDeviceName = "TARGET_DEVICE"
const EnvVarConfigFile = "CONFIG_FILE"

const ObjCopy = "objcopy"

type Configuration struct {
	ProjectDirectory     string
	OutputDirectory      string
	ProjectConfiguration *ProjectConfiguration
	DeviceConfiguration  runtime_config.DeviceConfig
}

func (c Configuration) ProjectDir() string {
	return c.ProjectDirectory
}

func (c Configuration) OutputDir() string {
	return c.OutputDirectory
}

func (c Configuration) ProjectConfig() runtime_config.ProjectConfigurationInterface {
	return c.ProjectConfiguration
}

func (c Configuration) DeviceConfig() runtime_config.DeviceConfig {
	return c.DeviceConfiguration
}

func (c Configuration) EnvConfig() runtime_config.EnvConfigurationInterface {
	return &c.ProjectConfiguration.Env
}

type EnvConfig struct {
	HostCC       string   `yaml:"host_cc"`
	HostCXX      string   `yaml:"host_cxx"`
	HostAS       string   `yaml:"host_as"`
	HostLD       string   `yaml:"host_ld"`
	HostCFLAGS   []string `yaml:"host_cflags"`
	HostCXXFLAGS []string `yaml:"host_cxxflags"`
	HostASFLAGS  []string `yaml:"host_asflags"`
	HostLDFLAGS  []string `yaml:"host_ldflags"`
	Go           string   `yaml:"go"`
	Shell        string   `yaml:"shell"`
	Make         string   `yaml:"make"`
}

func (e *EnvConfig) NativeHostCC() string {
	return e.HostCC
}

func (e *EnvConfig) NativeHostCXX() string {
	return e.HostCXX
}

func (e *EnvConfig) NativeHostLD() string {
	return e.HostLD
}

func (e *EnvConfig) NativeHostAS() string {
	return e.HostAS
}

func (e *EnvConfig) NativeHostCFlags() []string {
	return e.HostCFLAGS
}

func (e *EnvConfig) NativeHostCXXFlags() []string {
	return e.HostCXXFLAGS
}

func (e *EnvConfig) NativeHostLDFlags() []string {
	return e.HostLDFLAGS
}

func (e *EnvConfig) NativeHostASFlags() []string {
	return e.HostASFLAGS
}

func (e *EnvConfig) GoCommand() string {
	return e.Go
}

func (e *EnvConfig) MakeCommand() string {
	return e.Make
}

func (e *EnvConfig) ShellCommand() string {
	return e.Shell
}

type ProjectConfiguration struct {
	Env            EnvConfig             `yaml:"env"`
	TargetsToBuild []defs.RuleIdentifier `yaml:"targets"`
	RustNightly    bool                  `yaml:"rust_nightly_channel"`
	ShouldUseRust  bool                  `yaml:"use_rust"`
}

func (p *ProjectConfiguration) Targets() []defs.RuleIdentifier {
	return p.TargetsToBuild
}

func (p *ProjectConfiguration) FillDefaultsAndValidate() {
	if len(p.Env.HostCC) == 0 {
		p.Env.HostCC = "clang"
	}
	if len(p.Env.HostCXX) == 0 {
		p.Env.HostCXX = "clang"
	}
	if len(p.Env.HostAS) == 0 {
		p.Env.HostAS = "llvm-as"
	}
	if len(p.Env.HostLD) == 0 {
		p.Env.HostLD = "lld"
	}

	if len(p.Env.HostLDFLAGS) == 0 {
		p.Env.HostLDFLAGS = []string{"-O1"}
	}
	if len(p.Env.HostCFLAGS) == 0 {
		p.Env.HostCFLAGS = []string{"-O3"}
	}
	if len(p.Env.HostCXXFLAGS) == 0 {
		p.Env.HostCXXFLAGS = []string{"-O3"}
	}

	if len(p.Env.Go) == 0 {
		p.Env.Go = "go"
	}

	if len(p.Env.Shell) == 0 {
		p.Env.Shell = "bash"
	}

	if len(p.Env.Make) == 0 {
		p.Env.Make = "make"
	}

	defs.ProjectConfiguration = p
}

func (p ProjectConfiguration) String() string {
	byt, _ := yaml.Marshal(p)
	return string(byt)
}

func (e *EnvConfig) Assembler(buildFor defs.BuildFor, providerId string) string {
	switch buildFor {
	case defs.Host:
		return e.HostAS
	case defs.Target, defs.OSTarget:
		cct := provider.CrossToolchainProviderById(providerId)
		return cct.Prefix + cct.Programs.AS
	default:
		panic("invalid buildFor")
	}
}

func (e *EnvConfig) CCompiler(buildFor defs.BuildFor, providerId string) string {
	switch buildFor {
	case defs.Host:
		return e.HostCC
	case defs.Target, defs.OSTarget:
		cct := provider.CrossToolchainProviderById(providerId)
		return cct.Prefix + cct.Programs.CC
	default:
		panic("invalid buildFor")
	}
}

func (e *EnvConfig) CXXCompiler(buildFor defs.BuildFor, providerId string) string {
	switch buildFor {
	case defs.Host:
		return e.HostCXX
	case defs.Target, defs.OSTarget:
		cct := provider.CrossToolchainProviderById(providerId)
		return cct.Prefix + cct.Programs.CXX
	default:
		panic("invalid buildFor")
	}
}

func (e *EnvConfig) Linker(buildFor defs.BuildFor, providerId string) string {
	switch buildFor {
	case defs.Host:
		return e.HostLD
	case defs.Target, defs.OSTarget:
		cct := provider.CrossToolchainProviderById(providerId)
		return cct.Prefix + cct.Programs.LD
	default:
		panic("invalid buildFor")
	}
}

func (e *EnvConfig) ObjCopy(buildFor defs.BuildFor, providerId string) string {
	switch buildFor {
	case defs.Host:
		return ObjCopy
	case defs.Target, defs.OSTarget:
		cct := provider.CrossToolchainProviderById(providerId)
		return cct.Prefix + ObjCopy
	default:
		panic("invalid buildFor")
	}
}

func (e *EnvConfig) CFlags(buildFor defs.BuildFor) []string {
	switch buildFor {
	case defs.Host:
		return e.HostCFLAGS
	default:
		panic("invalid buildFor")
	}
}

func (e *EnvConfig) CXXFlags(buildFor defs.BuildFor) []string {
	switch buildFor {
	case defs.Host:
		return e.HostCXXFLAGS
	default:
		panic("invalid buildFor")
	}
}

func (e *EnvConfig) LDFlags(buildFor defs.BuildFor) []string {
	switch buildFor {
	case defs.Host:
		return e.HostLDFLAGS
	default:
		panic("invalid buildFor")
	}
}

func (e *EnvConfig) ASFlags(buildFor defs.BuildFor) []string {
	switch buildFor {
	case defs.Host:
		return e.HostASFLAGS
	default:
		panic("invalid buildFor")
	}
}

func (c *Configuration) ReadDeviceConfig(deviceName string) {
	deviceConfigs := fs.FindFilesLazily(path.Join(c.ProjectDir(), "device"), defs.DeviceConfigFileRegex)

	fmt.Printf("Device configs found: %v\n", deviceConfigs)

	foundConfig := false
	for _, deviceConfigFile := range deviceConfigs {
		f, err := os.Open(deviceConfigFile)
		if err != nil {
			panic(err)
		}

		configContent, err := io.ReadAll(f)
		if err != nil {
			panic(err)
		}
		_ = f.Close()

		var deviceConfig runtime_config.DeviceConfig
		err = yaml.Unmarshal(configContent, &deviceConfig)
		if err != nil {
			panic(err)
		}

		if deviceConfig.DeviceName == deviceName {
			foundConfig = true
			c.DeviceConfiguration = deviceConfig
			break
		}
	}
	if !foundConfig {
		panic("Config for device " + deviceName + " does not exist")
	}

	if len(c.DeviceConfiguration.DeviceName) == 0 {
		panic("No device name specified")
	}
	if len(c.DeviceConfiguration.Architecture) == 0 {
		panic("No architecture specified for device")
	}
	if !defs.IsArchitectureSupported(c.DeviceConfiguration.Architecture.String()) {
		panic("Unsupported architecture " + c.DeviceConfiguration.Architecture.String())
	}
	if len(c.DeviceConfiguration.CrossCompileRuleName) == 0 {
		panic("No cross compile rule ID specified")
	}

	c.DeviceConfiguration.Architecture = c.DeviceConfiguration.Architecture.Normalize()
}

func IsArchInList(arches []defs.Architecture) bool {
	for _, arch := range arches {
		if arch.Normalize() == runtime_config.Config.DeviceConfig().Architecture {
			return true
		}
	}
	return false
}

func (p *ProjectConfiguration) RustChannel() string {
	if p.RustNightly {
		return "nightly"
	} else {
		return "stable"
	}
}

func (p *ProjectConfiguration) UseRust() bool {
	return p.ShouldUseRust
}

var _ runtime_config.EnvConfigurationInterface = (*EnvConfig)(nil)

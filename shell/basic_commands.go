package shell

import "fmt"

func DirNameCommand(filePath string) string {
	return fmt.Sprintf("dirname %s", filePath)
}

func BaseNameCommand(filePath string) string {
	return fmt.Sprintf("basename %s", filePath)
}

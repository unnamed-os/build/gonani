package shell

import (
	"fmt"
	"strings"
)

type Arguments []Argument

type Command = Formattable
type Argument = Formattable

type Formattable struct {
	Format string
	Args   []interface{}
}

func (f Formattable) String() string {
	return fmt.Sprintf(f.Format, f.Args...)
}

func (a Arguments) String() string {
	return strings.Join(a.Args(), " ")
}

func (a Arguments) Args() []string {
	var args []string
	for _, arg := range a {
		args = append(args, arg.String())
	}
	return args
}

func (a *Arguments) AppendStringsIf(cond bool, strings ...string) *Arguments {
	if cond {
		a.AppendStrings(strings...)
	}
	return a
}

func (a *Arguments) AppendStrings(strings ...string) *Arguments {
	for _, str := range strings {
		*a = append(*a, Argument{Format: str})
	}
	return a
}

func ArgumentsFromStrings(strings ...string) *Arguments {
	args := &Arguments{}
	args.AppendStrings(strings...)
	return args
}

func (a *Arguments) Add(arg Argument) {
	*a = append(*a, arg)
}

func CommandString(str string) Command {
	return Command{
		Format: str,
	}
}

func EscapeForNinja(str string) string {
	return strings.ReplaceAll(str, "$", "$$")
}

func OutputOfCmd(cmd string) string {
	return fmt.Sprintf("`%s`", cmd)
}

func OutputOfCmdAlt(cmd string) string {
	return EscapeForNinja(fmt.Sprintf("$(%s)", cmd))
}

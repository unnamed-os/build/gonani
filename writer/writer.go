package writer

import (
	"fmt"
	"gonani/ninja"
	"gonani/rule"
	"gonani/targets/target_list"
)

type Writer struct {
	OutputDir   string
	RuleChan    chan *rule.Rule
	ninjaWriter ninja.Writer
}

func (w *Writer) Write() {
	w.ninjaWriter = ninja.NewWriter(w.OutputDir)
	defer w.ninjaWriter.Close()
	for {
		recvRule, hasMore := <-w.RuleChan
		if !hasMore || recvRule == nil {
			break
		}
		fmt.Printf("Writing build rules for %s...\n", recvRule.Identifier)
		t := target_list.TargetForRule(recvRule, &w.ninjaWriter)
		w.ninjaWriter.WriteTargetBuildRules(t)
	}
}

package build_set

import (
	"gonani/collector"
	"gonani/rule"
	"gonani/writer"
)

type BuildSet struct {
	Rules rule.Rules

	Collector *collector.Collector
	Writer    *writer.Writer
}

func (bs *BuildSet) Process() {
	for {
		rules, hasMore := <-bs.Collector.RulesChan
		if !hasMore || rules == nil {
			break
		}
		for _, r := range rules {
			bs.Writer.RuleChan <- r
		}
	}
	bs.Writer.RuleChan <- nil
	close(bs.Writer.RuleChan)
}

package ninja

import (
	"fmt"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/target"
	"path"
	"path/filepath"
	"strings"
)

type Stringifier struct {
	OutputDir string
}

func (s *Stringifier) ConstructOutputDir(out target.OutputFile) string {
	abs := path.Join(runtime_config.ProjectDir(), s.OutputDir)
	str, err := filepath.Rel(runtime_config.ProjectDir(), path.Join(abs, out))
	if err != nil {
		panic(err)
	}
	return str
}

func (s *Stringifier) BuildTargetRule(
	out target.OutputFile, ruleName string, inputFiles []string,
	implicitDependencies ...string,
) string {
	if path.IsAbs(out) {
		panic(fmt.Sprintf("Absolute output file '%s' of '%s' not allowed", out, ruleName))
	}
	for _, inputFile := range inputFiles {
		if path.IsAbs(inputFile) {
			panic(fmt.Sprintf("Absolute input file '%s' of '%s' not allowed", inputFile, ruleName))
		}
	}
	str := fmt.Sprintf("build %s:", s.ConstructOutputDir(out))
	if len(ruleName) > 0 {
		str += " " + ruleName
	}
	if len(inputFiles) > 0 {
		str += " " + strings.Join(inputFiles, " ")
	}
	if len(implicitDependencies) > 0 {
		str += " | " + strings.Join(implicitDependencies, " ")
	}
	return str
}

func ImplicitDependencyTarget(out string, dependencies ...string) string {
	return fmt.Sprintf("build %s: | %s", out, strings.Join(dependencies, " "))
}

func PhonyDependencyTarget(out string, dependencies ...string) string {
	return fmt.Sprintf("build %s: phony %s", out, strings.Join(dependencies, " "))
}

func Variable(name string, value string) string {
	return fmt.Sprintf("%s = %s", name, value)
}

func RuleHeader(ruleName string) string {
	return fmt.Sprintf("rule %s", ruleName)
}

func Indent(count int, str string) string {
	ind := ""
	for i := 0; i < count; i++ {
		ind += "  "
	}
	return ind + str
}

func Command(command string) string {
	return Indent(1, fmt.Sprintf("command = %s", command))
}

func RuleDescription(desc string) string {
	return Indent(1, fmt.Sprintf("description = %s", desc))
}

func RuleDepFile(depFile string) string {
	return Indent(1, fmt.Sprintf("depfile = %s", depFile))
}

func RuleDeps() string {
	return Indent(1, "deps = gcc")
}

func Comment(format string, a ...interface{}) string {
	return fmt.Sprintf("# "+format, a...)
}

func Phony(in string) string {
	return "phony " + in
}

func BuildOutputFromIdentifier(id defs.RuleIdentifier) string {
	return "rule-id-" + id
}

func BuildOutputsFromIdentifiers(ids []defs.RuleIdentifier) []string {
	outs := make([]string, len(ids))

	for i, id := range ids {
		outs[i] = BuildOutputFromIdentifier(id)
	}

	return outs
}

package rule

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"path"
	"path/filepath"
	"text/template"
)

type Rule struct {
	Identifier       defs.RuleIdentifier    `yaml:"-"`
	Type             defs.TargetType        `yaml:"type"`
	BuildFor         defs.BuildFor          `yaml:"build_for"`
	Config           map[string]interface{} `yaml:",inline"`
	DependsOn        []defs.RuleIdentifier  `yaml:"depends_on"`
	RulePath         string                 `yaml:"-"`
	RuleDir          string                 `yaml:"-"`
	BuildFrom        string                 `yaml:"build_from"`
	SharedIdentifier defs.RuleIdentifier    `yaml:"shared_id"`
	EnvVars          map[string]string      `yaml:"env"`
	Description      string                 `yaml:"desc"`

	OverwriteOutDir            string `yaml:"-"`
	BuildFromIsProjectRelative bool   `yaml:"build_from_is_project_relative"`
}

func (r Rule) RelativeOutputDir() string {
	if !r.BuildFor.IsValid() {
		r.BuildFor = defs.Target
	}
	rulePart := path.Join(r.IdentifierForOutput())
	if r.BuildFor == defs.Target || r.BuildFor == defs.OSTarget {
		return path.Join(r.BuildFor.String(), runtime_config.DeviceName(), rulePart)
	} else {
		return path.Join(r.BuildFor.String(), rulePart)
	}
}

func (r *Rule) AbsoluteSourceDir() string {
	srcDir := r.SourceDir()
	if path.IsAbs(srcDir) {
		return srcDir
	} else {
		return path.Clean(path.Join(runtime_config.ProjectDir(), srcDir))
	}
}

func (r *Rule) RelativeSourceFileRelativeToProjectDir(p string) string {
	srcDir := r.SourceDir()
	if path.IsAbs(srcDir) {
		srcDir = fs.MustRel(runtime_config.ProjectDir(), srcDir)
	}
	return path.Join(srcDir, p)
}

func (r *Rule) RelativeSourceFileAbsolute(p string) string {
	return path.Join(r.AbsoluteSourceDir(), p)
}

func RelativeToProjectDir(path string) string {
	rel, err := filepath.Rel(runtime_config.ProjectDir(), path)
	if err != nil {
		panic(err)
	}
	return rel
}

func (r Rule) OutputDirFromProjectDir() string {
	return RelativeToProjectDir(r.AbsoluteOutputDir())
}

func (r Rule) OutputDirFromSourceDir() string {
	rel, _ := filepath.Rel(r.SourceDir(), r.AbsoluteOutputDir())
	return rel
}

func (r Rule) AbsoluteOutputDir() string {
	return path.Join(runtime_config.OutputDir(), r.RelativeOutputDir())
}

func (r Rule) SourceDirRelativeToProjectDir() string {
	abs, err := filepath.Abs(r.SourceDir())
	if err != nil {
		panic(err)
	}
	rel, err := filepath.Rel(runtime_config.ProjectDir(), abs)
	if err != nil {
		panic(err)
	}
	return rel
}

func (r Rule) String() string {
	byt, _ := json.Marshal(r)
	return fmt.Sprintf("Rule%s", string(byt))
}

func (r Rule) IdentifierForOutput() defs.RuleIdentifier {
	if len(r.SharedIdentifier) > 0 {
		return r.SharedIdentifier
	}
	return r.Identifier
}

func (r Rule) SourceDir() string {
	if len(r.BuildFrom) == 0 {
		return r.RuleDir
	}
	if path.IsAbs(r.BuildFrom) || r.BuildFromIsProjectRelative {
		return r.BuildFrom
	}
	return path.Join(r.RuleDir, r.BuildFrom)
}

func (r *Rule) SetRuleDir(dir string) {
	r.RuleDir = dir
}

func (r Rule) RuleDirOnly() string {
	return r.RuleDir
}

func (r Rule) Desc() string {
	return r.Description
}

func (r *Rule) MarkCrossCompilable() {
	if r.BuildFor.NeedsCrossCompiler() {
		r.DependsOn = append(r.DependsOn, runtime_config.Config.DeviceConfig().CrossCompileRuleName)
	}
}

func (r Rule) RelativeProjectDir() string {
	rel, _ := filepath.Rel(r.SourceDir(), runtime_config.ProjectDir())
	return rel
}

func (r Rule) RelativeProjectDirFromOutputDir() string {
	rel, _ := filepath.Rel(r.AbsoluteOutputDir(), runtime_config.ProjectDir())
	return rel
}

func (r Rule) FileInOutDirFromSourceDir(p string) string {
	return path.Join(r.OutputDirFromSourceDir(), p)
}

func (r Rule) FileInOutputDir(p string) string {
	return path.Join(runtime_config.RelOutputDir(), r.RelativeOutputDir(), p)
}

func (r Rule) FileInSourceDirRelativeToProjectDir(p string) string {
	return path.Join(r.SourceDirRelativeToProjectDir(), p)
}

type TemplateContext struct {
	OutputDir string
}

func (r *Rule) ProcessTemplate(tmplStr string) string {
	tmpl, err := template.New("rule_related_template").Parse(tmplStr)
	if err != nil {
		panic(err)
	}
	buf := bytes.Buffer{}
	err = tmpl.Execute(&buf, TemplateContext{
		OutputDir: r.AbsoluteOutputDir(),
	})
	if err != nil {
		panic(err)
	}
	return buf.String()
}

func (r Rule) ProcessTemplates(templates []string) []string {
	for i := range templates {
		templates[i] = r.ProcessTemplate(templates[i])
	}
	return templates
}

func MinimalRuleForId(ruleId defs.RuleIdentifier, buildFor defs.BuildFor) *Rule {
	return &Rule{Identifier: ruleId, BuildFor: buildFor}
}

func AbsoluteOutputDirForId(ruleId defs.RuleIdentifier, buildFor defs.BuildFor) string {
	return MinimalRuleForId(ruleId, buildFor).AbsoluteOutputDir()
}

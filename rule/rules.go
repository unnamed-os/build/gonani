package rule

import "gonani/defs"

type Rules = map[defs.RuleIdentifier]*Rule

package fs

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func DirectoryExists(path string) bool {
	stat, err := os.Stat(path)
	if err == nil {
		return stat.IsDir()
	}
	return false
}

func FileExists(path string) bool {
	stat, err := os.Stat(path)
	if err == nil {
		return !stat.IsDir()
	}
	return false
}

func AddExtension(filename string, ext string) string {
	return fmt.Sprintf("%s.%s", filename, ext)
}

func ReplaceExtension(filename string, ext string) string {
	return fmt.Sprintf("%s.%s", strings.TrimSuffix(filename, filepath.Ext(filename)), ext)
}

func RemoveExtension(filename string) string {
	return strings.TrimSuffix(filename, filepath.Ext(filename))
}

func MustRel(basePath string, targetPath string) string {
	rel, err := filepath.Rel(basePath, targetPath)
	if err != nil {
		panic(err)
	}
	return rel
}

func MustAbs(p string) string {
	abs, err := filepath.Abs(p)
	if err != nil {
		panic(err)
	}
	return abs
}

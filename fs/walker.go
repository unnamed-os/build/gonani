package fs

import (
	"fmt"
	"gonani/defs"
	"io"
	"os"
	"path/filepath"
)

type WalkerResult struct {
	Content []byte
	Err     error
	Path    string
}

func Walk(dir string) (resultChan chan WalkerResult) {
	resultChan = make(chan WalkerResult)
	go func() {
		lastPath := ""
		err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
			if !info.IsDir() && defs.RulesFileRegex.MatchString(info.Name()) {
				fmt.Printf("Reading build rules from %s\n", path)
				lastPath = path
				file, err := os.Open(path)
				if err != nil {
					return err
				}
				content, err := io.ReadAll(file)
				if err != nil {
					return err
				}
				resultChan <- WalkerResult{
					Content: content,
					Err:     nil,
					Path:    path,
				}
			}
			return err
		})
		resultChan <- WalkerResult{
			Content: nil,
			Err:     err,
			Path:    lastPath,
		}
		close(resultChan)
	}()
	return
}

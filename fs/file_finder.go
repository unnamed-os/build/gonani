package fs

import (
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
)

func FindFiles(dir string, pattern *regexp.Regexp, ignoreFileNames ...string) (filesFound []string) {
	err := filepath.Walk(dir, func(p string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && pattern.MatchString(info.Name()) {
			for _, ignore := range ignoreFileNames {
				if ignore == info.Name() {
					return nil
				}
			}
			abs, err := filepath.Abs(p)
			if err != nil {
				return err
			}
			rel, err := filepath.Rel(dir, abs)
			if err != nil {
				return err
			}
			filesFound = append(filesFound, rel)
		}
		return err
	})
	if err != nil {
		panic(err)
	}
	return
}

func FindFile(dir string, filename string) (fileFound string) {
	err := filepath.Walk(dir, func(p string, info os.FileInfo, err error) error {
		if len(fileFound) > 0 {
			return nil
		}
		if err != nil {
			return err
		}
		if !info.IsDir() && filename == info.Name() {
			abs, err := filepath.Abs(p)
			if err != nil {
				return err
			}
			rel, err := filepath.Rel(dir, abs)
			if err != nil {
				return err
			}
			fileFound = rel
			return nil
		}
		return err
	})
	if err != nil {
		panic(err)
	}
	return
}

func FindFilesExact(dir string, filename string) (filesFound []string) {
	err := filepath.Walk(dir, func(p string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && filename == info.Name() {
			abs, err := filepath.Abs(p)
			if err != nil {
				return err
			}
			rel, err := filepath.Rel(dir, abs)
			if err != nil {
				return err
			}
			filesFound = append(filesFound, rel)
		}
		return err
	})
	if err != nil {
		panic(err)
	}
	return
}

func FindPaths(dir string, pattern *regexp.Regexp) (filesFound []string) {
	err := filepath.Walk(dir, func(p string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && pattern.MatchString(p) {
			abs, err := filepath.Abs(p)
			if err != nil {
				return err
			}
			rel, err := filepath.Rel(dir, abs)
			if err != nil {
				return err
			}
			filesFound = append(filesFound, rel)
		}
		return err
	})
	if err != nil {
		panic(err)
	}
	return
}

func PathParts(p string) []string {
	return strings.Split(filepath.Clean(filepath.ToSlash(p)), "/")
}

func findFilesLazily(dir string, pattern *regexp.Regexp) (filesFound []string) {
	dirEnts, _ := os.ReadDir(dir)
	for _, dirEnt := range dirEnts {
		if !dirEnt.IsDir() && pattern.MatchString(dirEnt.Name()) {
			filesFound = append(filesFound, path.Join(dir, dirEnt.Name()))
		}
	}
	if len(filesFound) == 0 {
		for _, dirEnt := range dirEnts {
			if dirEnt.IsDir() {
				filesFound = append(filesFound, findFilesLazily(path.Join(dir, dirEnt.Name()), pattern)...)
			}
		}
	}
	return
}

func FindFilesLazily(dir string, pattern *regexp.Regexp) (filesFound []string) {
	filesFound = findFilesLazily(dir, pattern)

	return filesFound
}

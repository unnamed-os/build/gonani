package paths

import (
	"gonani/config/runtime_config"
	"gonani/defs"
	"path"
	"path/filepath"
)

func ProjectDirRelativeTo(dir string) string {
	abs, err := filepath.Abs(dir)
	if err != nil {
		panic(err)
	}
	rel, err := filepath.Rel(abs, runtime_config.ProjectDir())
	if err != nil {
		panic(err)
	}
	return rel
}

func ExportsDirOf(ruleId defs.RuleIdentifier) string {
	return path.Join(runtime_config.RelOutputDir(), "exports", ruleId)
}

func OuterHeaderDirOf(ruleId defs.RuleIdentifier) string {
	return path.Join(ExportsDirOf(ruleId), "headers")
}

func HeaderDirOf(ruleId defs.RuleIdentifier) string {
	return path.Join(OuterHeaderDirOf(ruleId), ruleId)
}

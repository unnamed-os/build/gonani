package target

import (
	"fmt"
	"gonani/collections"
	"gonani/config"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"gonani/provider"
	"gonani/rule"
	"gonani/shell"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

type OutputFile = string

type RebuildStrategy int

const (
	RebuildLazy RebuildStrategy = iota
	RebuildAlways
	RebuildNever
)
const RebuildDefault = RebuildLazy

type Target interface {
	UseRule(rule *rule.Rule)
	BuildSteps() BuildSteps
	BuildRuleNameExtras() string
	RebuildStrategy() RebuildStrategy
	ParseConfigIfNecessary()
	Rule() *rule.Rule
	SubTargets() []Target
	Description() string
	UseBuildRuleWriter(w BuildRuleWriter)
}

type CommandBuildStep interface {
	Command() shell.Command
	Arguments() shell.Arguments
	InputFiles() []string
	Description() string
	DepFile() string
	UseDeps() bool
}

type CommandBuildStepBase struct{}

func (b CommandBuildStepBase) Description() string {
	return ""
}

func (b CommandBuildStepBase) DepFile() string {
	return ""
}

func (b CommandBuildStepBase) UseDeps() bool {
	return false
}

type BuildSteps = map[OutputFile]CommandBuildStep

func Initialize(t Target, r *rule.Rule, w BuildRuleWriter) {
	t.UseRule(r)
	t.UseBuildRuleWriter(w)
	t.ParseConfigIfNecessary()
}

type Base struct {
	rule            *rule.Rule
	BuildRuleWriter BuildRuleWriter
}

func (t *Base) UseRule(r *rule.Rule) {
	t.rule = r
}

func (t *Base) Rule() *rule.Rule {
	return t.rule
}

func (t *Base) Description() string {
	r := t.Rule()
	if r != nil {
		return r.Description
	} else {
		return ""
	}
}

func (t *Base) UseBuildRuleWriter(w BuildRuleWriter) {
	t.BuildRuleWriter = w
}

func FindFileInOutputOfModuleCommand(t Target, moduleRuleId defs.RuleIdentifier, basename string, maxDepth int) string {
	outDir := rule.MinimalRuleForId(moduleRuleId, t.Rule().BuildFor).OutputDirFromProjectDir()
	maxDepthStr := ""
	if maxDepth > 0 {
		maxDepthStr = "-maxdepth " + strconv.Itoa(maxDepth)
	}
	return fmt.Sprintf("find '%s' %s -type f -name '%s'", outDir, maxDepthStr, basename)
}

type SubTargetsBase struct {
	subTargets []Target
}

func (s *SubTargetsBase) SubTargets() []Target {
	return s.subTargets
}

type SimpleTargetBase struct {
	Base
	SubTargetsBase
}

func (s *SimpleTargetBase) BuildRuleNameExtras() string {
	return ""
}

func (s *SimpleTargetBase) RebuildStrategy() RebuildStrategy {
	return RebuildDefault
}

func (s *SimpleTargetBase) ParseConfigIfNecessary() {

}

type SrcDirTargetBase struct {
	SrcDir string `yaml:"srcdir"`
}

type SrcFilesTargetBase struct {
	SrcFiles      []string `yaml:"srcs"`
	SrcFilesRegex []string `yaml:"srcs_regex"`
}

type HeaderFilesTargetBase struct {
	HeaderFiles []string `yaml:"headers"`
}

type OutputFilenameTargetBase struct {
	OutputFilename string `yaml:"out_filename"`
}

func (s SrcFilesTargetBase) AllSrcFiles(r *rule.Rule) []string {
	allFiles := s.SrcFiles

	for _, srcFileRegex := range s.SrcFilesRegex {
		regex, err := regexp.Compile(srcFileRegex)
		if err != nil {
			panic(err)
		}
		allFiles = append(allFiles, fs.FindPaths(r.SourceDir(), regex)...)
	}

	return allFiles
}

type UsingToolchainExtension struct {
	CrossToolchainId map[defs.Architecture]defs.RuleIdentifier `yaml:"cross_toolchain"`
	Rule             *rule.Rule
}

func (t UsingToolchainExtension) CrossToolchainID() defs.RuleIdentifier {
	if _, hasSpecified := t.CrossToolchainId[runtime_config.DeviceArchitecture()]; !hasSpecified {
		return runtime_config.Config.DeviceConfig().CrossCompileRuleName
	} else {
		return t.CrossToolchainId[runtime_config.DeviceArchitecture()]
	}
}

func (t UsingToolchainExtension) CCompiler() string {
	return t.Toolchain().Prefix + t.Toolchain().Programs.CC
}

func (t UsingToolchainExtension) CXXCompiler() string {
	return t.Toolchain().Prefix + t.Toolchain().Programs.CXX
}

func (t UsingToolchainExtension) Assembler() string {
	return t.Toolchain().Prefix + t.Toolchain().Programs.AS
}

func (t UsingToolchainExtension) Linker() string {
	return t.Toolchain().Prefix + t.Toolchain().Programs.LD
}

func (t UsingToolchainExtension) ObjCopy() string {
	return t.Toolchain().Prefix + config.ObjCopy
}

func (t UsingToolchainExtension) Toolchain() *provider.CrossToolchainProvider {
	if !t.Rule.BuildFor.NeedsCrossCompiler() {
		return provider.HostToolchainProvider()
	}
	return provider.CrossToolchainProviderById(t.CrossToolchainID())
}

func (t UsingToolchainExtension) ToolchainProgramPath(program string) string {
	tc := t.Toolchain()
	if !t.Rule.BuildFor.NeedsCrossCompiler() || tc.IsHost {
		return tc.Prefix + program
	}
	return path.Join(
		rule.Rule{Identifier: t.CrossToolchainID(), BuildFor: defs.Host}.OutputDirFromProjectDir(),
		tc.BinDir, tc.Prefix+program)
}

func (t UsingToolchainExtension) CCompilerPath() string {
	return t.ToolchainProgramPath(t.Toolchain().Programs.CC)
}

func (t UsingToolchainExtension) CXXCompilerPath() string {
	return t.ToolchainProgramPath(t.Toolchain().Programs.CXX)
}

func (t UsingToolchainExtension) AssemblerPath() string {
	return t.ToolchainProgramPath(t.Toolchain().Programs.AS)
}

func (t UsingToolchainExtension) LinkerPath() string {
	return t.ToolchainProgramPath(t.Toolchain().Programs.LD)
}

func (t UsingToolchainExtension) ObjCopyPath() string {
	return t.ToolchainProgramPath(config.ObjCopy)
}

func DecodeConfig(t Target, r *rule.Rule) error {
	err := defs.ConfigDecoder(t).Decode(r.Config)
	if err != nil {
		return err
	}
	return nil
}

func (h *HeaderFilesTargetBase) FillDefaults() {

}

func (s *SrcDirTargetBase) FillDefaults() {
	if len(s.SrcDir) == 0 {
		s.SrcDir = "."
	}
}

func (s *SrcDirTargetBase) ProjectRelSrcDir(r *rule.Rule) string {
	abs, _ := filepath.Abs(r.SourceDir())
	rel, _ := filepath.Rel(runtime_config.ProjectDir(), filepath.Join(abs, s.SrcDir))
	return rel
}

func (s *SrcDirTargetBase) AbsSrcDir(r *rule.Rule) string {
	abs, _ := filepath.Abs(filepath.Join(r.SourceDir(), s.SrcDir))
	return abs
}

func EnvVarString(vars map[string]string, r *rule.Rule) string {
	str := ""
	collections.IterSorted(vars, func(key interface{}, value interface{}) {
		str += fmt.Sprintf("export %s=%s; ", key.(string), shell.EscapeForNinja(r.ProcessTemplate(value.(string))))
	})
	str = strings.TrimRight(str, " ")
	return str
}

func VarString(vars map[string]string, r *rule.Rule) string {
	str := ""
	collections.IterSorted(vars, func(key interface{}, value interface{}) {
		str += fmt.Sprintf("%s=%s ", key.(string), shell.EscapeForNinja(r.ProcessTemplate(value.(string))))
	})
	str = strings.TrimRight(str, " ")
	return str
}

func CommandBuildStepString(c CommandBuildStep, r *rule.Rule) string {
	return fmt.Sprintf("%s ( %s %s )", EnvVarString(r.EnvVars, r), c.Command(), c.Arguments())
}

func RelativeToProjectDir(path string) string {
	return rule.RelativeToProjectDir(path)
}

func AllSourceFilesRelativeToProjectDir(r *rule.Rule, paths ...string) []string {
	var relPaths []string
	for _, p := range paths {
		relPaths = append(relPaths, r.RelativeSourceFileRelativeToProjectDir(p))
	}
	return relPaths
}

func BuildStepRelInputFiles(r *rule.Rule, b CommandBuildStep) []string {
	return AllSourceFilesRelativeToProjectDir(r, b.InputFiles()...)
}

func PathAddPrefixAll(prefix string, arr []string) []string {
	var newArr []string
	for _, p := range arr {
		newArr = append(newArr, path.Join(prefix, p))
	}
	return newArr
}

type BuildRuleWriter interface {
	WriteTargetBuildRules(t Target)
}

func FileDependency(file string) string {
	return defs.FileDependencyPrefix + file
}

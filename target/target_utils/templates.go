package target_utils

import (
	"bytes"
	"html/template"
)

func ProcessTemplate(tmplStr string, bits interface{}) string {
	tmpl, err := template.New("target_specific_template").Parse(tmplStr)
	if err != nil {
		panic(err)
	}
	buf := bytes.Buffer{}
	err = tmpl.Execute(&buf, bits)
	if err != nil {
		panic(err)
	}
	return buf.String()
}

module gonani

go 1.17

require (
	github.com/fatih/color v1.12.0 // indirect
	github.com/goccy/go-yaml v1.9.2
	github.com/mitchellh/mapstructure v1.4.1
	github.com/pkg/errors v0.9.1
	gitlab.com/xdevs23/go-runtimeutil v1.0.0
	golang.org/x/sys v0.0.0-20210514084401-e8d321eab015
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)

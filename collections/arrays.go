package collections

import (
	"path"
	"strings"
)

func HasString(arr []string, str string) bool {
	for _, e := range arr {
		if e == str {
			return true
		}
	}
	return false
}

func HasStringSuffix(arr []string, suffix string) bool {
	for _, e := range arr {
		if strings.HasSuffix(e, suffix) {
			return true
		}
	}
	return false
}

func HasStringPrefix(arr []string, prefix string) bool {
	for _, e := range arr {
		if strings.HasPrefix(e, prefix) {
			return true
		}
	}
	return false
}

func HasAnyOfStringPrefixes(str string, prefixes []string) bool {
	for _, prefix := range prefixes {
		if strings.HasPrefix(str, prefix) {
			return true
		}
	}
	return false
}

func FindStringBySuffix(arr []string, suffix string) string {
	for _, e := range arr {
		if strings.HasSuffix(e, suffix) {
			return e
		}
	}
	return ""
}

func FindStringIndex(arr []string, str string) int {
	for i, e := range arr {
		if e == str {
			return i
		}
	}
	return -1
}

func RemoveStringFromSlice(arr []string, str string) []string {
	var newArr []string
	for _, e := range arr {
		if e != str {
			newArr = append(newArr, e)
		}
	}
	return newArr
}

func MoveStringInSliceToFront(arr []string, str string) []string {
	return append([]string{str}, RemoveStringFromSlice(arr, str)...)
}

func MoveStringInSliceToBack(arr []string, str string) []string {
	return append(RemoveStringFromSlice(arr, str), str)
}

func AddPrefixToSliceAll(slice []string, prefix string) []string {
	for index := range slice {
		slice[index] = prefix + slice[index]
	}
	return slice
}

func JoinPathsWithPrefixAll(allPaths []string, prefix string) []string {
	for index := range allPaths {
		allPaths[index] = path.Join(prefix, allPaths[index])
	}
	return allPaths
}

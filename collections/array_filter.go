package collections

import "reflect"

func FilterSlice(slice interface{}, filterFunc func(entry interface{}) bool) interface{} {
	sliceTyp := reflect.TypeOf(slice)
	if sliceTyp.Kind() != reflect.Slice {
		panic("Need slice")
	}
	sliceVal := reflect.ValueOf(slice)

	typeOfSlice := sliceTyp.Elem()

	filteredSliceVal := reflect.MakeSlice(reflect.SliceOf(typeOfSlice), 0, sliceVal.Len())

	filterI := 0
	for i := 0; i < sliceVal.Len(); i++ {
		val := sliceVal.Index(i)
		if filterFunc(val.Interface()) {
			filteredSliceVal.Index(filterI).Set(val)
			filterI++
		}
	}

	finalFilteredSliceVal := reflect.MakeSlice(reflect.SliceOf(typeOfSlice), filterI, filterI)
	reflect.Copy(finalFilteredSliceVal, filteredSliceVal)

	return finalFilteredSliceVal.Interface()
}

func FilterOutSlice(slice interface{}, filterOutFunc func(entry interface{}) bool) interface{} {
	sliceTyp := reflect.TypeOf(slice)
	if sliceTyp.Kind() != reflect.Slice {
		panic("Need slice")
	}
	sliceVal := reflect.ValueOf(slice)

	typeOfSlice := sliceTyp.Elem()

	filteredSliceVal := reflect.MakeSlice(reflect.SliceOf(typeOfSlice), sliceVal.Len(), sliceVal.Len())

	filterI := 0
	for i := 0; i < sliceVal.Len(); i++ {
		val := sliceVal.Index(i)
		if !filterOutFunc(val.Interface()) {
			filteredSliceVal.Index(filterI).Set(val)
			filterI++
		}
	}

	finalFilteredSliceVal := reflect.MakeSlice(reflect.SliceOf(typeOfSlice), filterI, filterI)
	reflect.Copy(finalFilteredSliceVal, filteredSliceVal)

	return finalFilteredSliceVal.Interface()
}

func FindInSlice(slice interface{}, findFunc func(entry interface{}) bool) interface{} {
	sliceTyp := reflect.TypeOf(slice)
	if sliceTyp.Kind() != reflect.Slice {
		panic("Need slice")
	}
	sliceVal := reflect.ValueOf(slice)

	for i := 0; i < sliceVal.Len(); i++ {
		val := sliceVal.Index(i)
		iFace := val.Interface()
		if findFunc(iFace) {
			return iFace
		}
	}

	return nil
}

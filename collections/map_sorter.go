package collections

import (
	"reflect"
	"sort"
)

func IterSorted(m interface{}, iterFunc func(key interface{}, value interface{})) {
	mapTyp := reflect.TypeOf(m)
	if mapTyp.Kind() != reflect.Map {
		panic("m is not a map!")
	}

	mapVal := reflect.ValueOf(m)
	keyValues := mapVal.MapKeys()
	if len(keyValues) > 0 {
		firstKey := keyValues[0]
		keyType := firstKey.Type()

		keys := reflect.MakeSlice(reflect.SliceOf(keyType), len(keyValues), len(keyValues))
		for i := 0; i < keys.Len(); i++ {
			keys.Index(i).Set(keyValues[i])
		}

		switch {
		case keyType.AssignableTo(reflect.TypeOf((*string)(nil)).Elem()):
			sort.Strings(keys.Interface().([]string))
		case keyType.AssignableTo(reflect.TypeOf((*int)(nil)).Elem()):
			sort.Ints(keys.Interface().([]int))
		default:
			panic("unsupported type " + keyType.String())
		}

		for i := 0; i < keys.Len(); i++ {
			iterFunc(keys.Index(i).Interface(), mapVal.MapIndex(keys.Index(i)).Interface())
		}
	}
}

package main

import (
	"fmt"
	"gonani/build_set"
	"gonani/cmds"
	"gonani/collector"
	"gonani/config"
	"gonani/config/runtime_config"
	"gonani/fs"
	"gonani/provider"
	"gonani/rule"
	"gonani/tools/rustup"
	"gonani/writer"
	"io"
	"os"
	"path"
	"path/filepath"
	"sync"

	"golang.org/x/sys/execabs"

	"gopkg.in/yaml.v3"
)

var wg sync.WaitGroup

var copyright = []string{
	"gonani Ninja Build File Generator  Copyright (C) 2021  Simão Gomes Viana",
	"This program comes with ABSOLUTELY NO WARRANTY; for details vie the LICENSE file that came with the source code.",
	"This is free software, and you are welcome to redistribute it under certain conditions.",
}

func usage() {
	for _, line := range copyright {
		fmt.Println(line)
	}
	fmt.Println()
	fmt.Printf("Usage: %s <project dir> <output dir>\n", os.Args[0])
	fmt.Printf("\nEnvironment variables:\n")
	fmt.Printf("\t%s (required)\n", config.EnvVarDeviceName)
	fmt.Printf("\t%s (required, default: %s)\t\tRelative to project dir\n",
		config.EnvVarConfigFile, config.DefaultConfigFile)
	fmt.Println()
}

func main() {
	if len(os.Args) < 1+2 {
		usage()
		os.Exit(1)
	}

	targetDevice, exists := os.LookupEnv(config.EnvVarDeviceName)
	if !exists {
		panic("No TARGET_DEVICE specified")
	}
	fmt.Printf("Target device: %s\n", targetDevice)

	configFile := config.DefaultConfigFile
	if value, exists := os.LookupEnv(config.EnvVarConfigFile); exists {
		configFile = value
	}
	fmt.Printf("Config file: %s\n", configFile)

	projectDir := os.Args[1]
	outputDir := os.Args[2]

	if !fs.DirectoryExists(projectDir) {
		fmt.Printf("Cannot access project directory %s\n", projectDir)
		os.Exit(2)
	}

	err := os.MkdirAll(outputDir, 0750)
	if err != nil {
		panic(err)
	}

	if !fs.DirectoryExists(outputDir) {
		fmt.Printf("Cannot access output directory %s\n", outputDir)
		os.Exit(2)
	}

	absOutputDir, err := filepath.Abs(outputDir)
	if err != nil {
		panic(err)
	}

	absProjectDir, err := filepath.Abs(projectDir)
	if err != nil {
		panic(err)
	}

	pwd, _ := filepath.Abs(".")
	relPwd, _ := filepath.Rel(pwd, absProjectDir)
	configFile = path.Join(relPwd, configFile)
	if !fs.FileExists(configFile) {
		usage()
		panic("Cannot access config file " + configFile)
	}

	cfg := &config.Configuration{
		ProjectDirectory: absProjectDir,
		OutputDirectory:  absOutputDir,
	}
	runtime_config.Config = cfg

	configF, err := os.Open(configFile)
	if err != nil {
		panic(err)
	}
	configFileContent, err := io.ReadAll(configF)
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal(configFileContent, &cfg.ProjectConfiguration)
	if err != nil {
		panic(err)
	}

	provider.FindAndPopulateProviders()

	runtime_config.Config.ProjectConfig().FillDefaultsAndValidate()

	runtime_config.Config.ReadDeviceConfig(targetDevice)

	if runtime_config.Config.ProjectConfig().UseRust() {
		setupRust()
	}

	collector := &collector.Collector{
		ProjectDir: absProjectDir,
		RulesChan:  make(chan rule.Rules),
	}
	writer := &writer.Writer{
		OutputDir: absOutputDir,
		RuleChan:  make(chan *rule.Rule),
	}
	rules := &build_set.BuildSet{
		Collector: collector,
		Writer:    writer,
	}

	wg.Add(3)
	go func() {
		rules.Process()
		wg.Done()
	}()
	go func() {
		collector.Collect()
		wg.Done()
	}()
	go func() {
		writer.Write()
		wg.Done()
	}()

	wg.Wait()

	fmt.Println("Finished writing build rules")

	fmt.Println("Everything is ready")
}

func setupRust() {
	var err error

	fmt.Println("Checking rust...")
	rustup.ProgramPath, err = execabs.LookPath("rustup")
	if err != nil {
		fmt.Println("Cannot find rustup, please install it to do cross-compilation")
		panic("cannot run rustup")
	}

	fmt.Println("Installing rust toolchain...")
	err = cmds.RunCommand(rustup.ProgramPath, "toolchain", "install",
		runtime_config.Config.ProjectConfig().RustChannel())
	if err != nil {
		fmt.Println("Error while installing rust toolchain, channel",
			runtime_config.Config.ProjectConfig().RustChannel())
		panic("cannot install rust toolchain")
	}

	fmt.Println("Installing additional rust components...")
	rustup.InstallRustComponent("rust-src")
}

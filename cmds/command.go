package cmds

import (
	"fmt"
	"os"
	"os/exec"
)

func RunCommand(path string, args ...string) error {
	cmd := exec.Command(path, args...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	fmt.Println("Running command:", cmd.String())
	return cmd.Run()
}

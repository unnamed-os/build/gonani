package provider

import (
	"gonani/collections"
	"gonani/defs"
)

var providers = map[Providable][]Provider{}

func Providers() map[Providable][]Provider {
	return providers
}

func ProvidersProviding(providable Providable) []Provider {
	return Providers()[providable]
}

func ByIdentifier(providable Providable, id defs.RuleIdentifier) Provider {
	return collections.FindInSlice(ProvidersProviding(providable), func(entry interface{}) bool {
		return entry.(Provider).ProviderBase().Identifier == id
	}).(Provider)
}

package provider

import (
	"errors"
	"fmt"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/tools"
	"gonani/tools/toolchains"
	"regexp"
	"strconv"
	"strings"

	"golang.org/x/sys/execabs"
)

type ToolchainVendor string

const (
	GCC  ToolchainVendor = "gcc"
	LLVM ToolchainVendor = "llvm"
)

type CrossToolchainPrograms struct {
	AS  string `yaml:"as"`
	CC  string `yaml:"cc"`
	CXX string `yaml:"cxx"`
	LD  string `yaml:"ld"`
}

type CrossToolchainProvider struct {
	Base      `yaml:",inline"`
	Prefix    string
	Programs  CrossToolchainPrograms `yaml:"programs"`
	BinDir    string                 `yaml:"bin_dir"`
	Vendor    ToolchainVendor        `yaml:"vendor"`
	IsHost    bool                   `yaml:"host"`
	Options   map[string]interface{} `yaml:"options"`
	Toolchain tools.NativeToolchain  `yaml:"-"`
}

func llvmDetectVersionAndPath(programBase string, options toolchains.LLVMOptions) (string, error) {
	var versions []string
	for version := options.LLVMMaxVersion; version >= options.LLVMMinVersion; version-- {
		program := fmt.Sprintf("%s-%d", programBase, version)
		path, err := execabs.LookPath(program)
		if err == nil && len(path) > 0 {
			fmt.Printf("Detected LLVM program %s version %d: %s\n", programBase, version, path)
			return path, nil
		}
		versions = append(versions, strconv.Itoa(version))
	}
	path, err := execabs.LookPath(programBase)
	if err == nil {
		cmd := execabs.Command(path, "--version")
		output, err := cmd.CombinedOutput()
		if err != nil {
			fmt.Printf(`Error (%v) running "%s": %s`+"\n", err, cmd.String(), string(output))
			goto error
		}
		var llvmVersionRegex = regexp.MustCompile(
			"[A-Za-z0-9._-](\t| )+(" + strings.Join(versions, "|") + ")[.][0-9]+[.][0-9]+.*")
		if !llvmVersionRegex.Match(output) {
			fmt.Printf(`Command "%s" is not of compatible version in range %d-%d: %s`+"\n",
				cmd.String(), options.LLVMMinVersion, options.LLVMMaxVersion, string(output))
			goto error
		}

		fmt.Printf("Using default LLVM program \"%s\", version %s\n", path,
			string(llvmVersionRegex.FindAllSubmatch(output, 2)[0][2]))
		return path, nil
	}

error:
	return "", errors.New("could not find LLVM program")
}

func llvmDetectVersionForProgram(program *string, llvmTc *toolchains.LLVMToolchain) {
	var err error
	programBackup := *program
	if *program, err = llvmDetectVersionAndPath(*program, llvmTc.Options); err != nil {
		panic(
			fmt.Sprintf(
				"Cannot find LLVM version of \"%s\" (range %d-%d)",
				programBackup, llvmTc.Options.LLVMMinVersion, llvmTc.Options.LLVMMaxVersion,
			),
		)
	}
}

func CrossToolchainProviderById(id defs.RuleIdentifier) *CrossToolchainProvider {
	tc := ByIdentifier(CrossCompileToolchain, id).(*CrossToolchainProvider)
	if tc.Toolchain == nil {
		switch tc.Vendor {
		case GCC:
			tc.Toolchain = &toolchains.GCCToolchain{}
		case LLVM:
			llvmTc := &toolchains.LLVMToolchain{}
			defs.MustDecodeConfig(&llvmTc.Options, &tc.Options)
			if llvmTc.Options.LLVMAutoDetectVersion {
				llvmDetectVersionForProgram(&tc.Programs.CC, llvmTc)
				llvmDetectVersionForProgram(&tc.Programs.CXX, llvmTc)
				llvmDetectVersionForProgram(&tc.Programs.AS, llvmTc)
				llvmDetectVersionForProgram(&tc.Programs.LD, llvmTc)
			}
			tc.Toolchain = llvmTc
		default:
			panic("Unsupported toolchain vendor " + tc.Vendor)
		}
	}
	return tc
}

var hostToolchainProvider *CrossToolchainProvider

func HostToolchainProvider() *CrossToolchainProvider {
	if hostToolchainProvider == nil {
		hostToolchainProvider = &CrossToolchainProvider{
			Base:   Base{},
			Prefix: "",
			Programs: CrossToolchainPrograms{
				CC:  runtime_config.Config.EnvConfig().NativeHostCC(),
				CXX: runtime_config.Config.EnvConfig().NativeHostCXX(),
				LD:  runtime_config.Config.EnvConfig().NativeHostLD(),
				AS:  runtime_config.Config.EnvConfig().NativeHostAS(),
			},
			BinDir: "",
			IsHost: true,
		}
	}
	return hostToolchainProvider
}

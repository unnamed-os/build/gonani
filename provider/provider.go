package provider

import (
	"gonani/defs"
)

type Providable string

const (
	CrossCompileToolchain Providable = "cross-compile-toolchain"
)

type Provider interface {
	ProviderBase() Base
}

type Base struct {
	Identifier defs.RuleIdentifier `yaml:"rule_id"`
	Provides   Providable          `yaml:"provides"`
}

func (b Base) ProviderBase() Base {
	return b
}

func (p Providable) CreateProvider() Provider {
	switch p {
	case CrossCompileToolchain:
		return &CrossToolchainProvider{}
	default:
		panic("Invalid providable " + p)
	}
}

var _ Provider = (*Base)(nil)

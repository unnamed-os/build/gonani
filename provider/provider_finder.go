package provider

import (
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"io"
	"os"

	"gopkg.in/yaml.v3"
)

func FindAndPopulateProviders() {
	providerFiles := fs.FindFilesLazily(runtime_config.Config.ProjectDir(), defs.ProviderFileRegex)

	for _, providerFile := range providerFiles {
		f, err := os.Open(providerFile)
		if err != nil {
			panic(err)
		}

		configContent, err := io.ReadAll(f)
		if err != nil {
			panic(err)
		}
		_ = f.Close()

		var providerBase Base
		err = yaml.Unmarshal(configContent, &providerBase)
		if err != nil {
			panic(err)
		}
		if len(providerBase.Provides) == 0 {
			panic("missing provides in provider")
		}
		if len(providerBase.Identifier) == 0 {
			panic("missing rule id in provider")
		}

		provider := providerBase.Provides.CreateProvider()
		err = yaml.Unmarshal(configContent, provider)
		if err != nil {
			panic(err)
		}

		providers[providerBase.Provides] = append(providers[providerBase.Provides], provider)
	}
}

package targets

import (
	"fmt"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
	"gonani/tools/rustup"
	"path"
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

var rustBinarySourceFileRegex = regexp.MustCompile("^(.*)[.]rs$")

type RustBinaryTarget struct {
	target.Base
	RustBinaryConfig

	rule       *rule.Rule
	isComplete bool
}

type RustBinaryConfig struct {
	CargoFlags []string                       `yaml:"cargo_flags"`
	RustCFlags map[defs.Architecture][]string `yaml:"rustc_flags"`
	OutputType string                         `yaml:"output_type"`

	step *RustBinaryBuildStep
}

func (r *RustBinaryTarget) SubTargets() []target.Target {
	return nil
}

func (r *RustBinaryTarget) RebuildStrategy() target.RebuildStrategy {
	return target.RebuildLazy
}

func (r *RustBinaryTarget) Rule() *rule.Rule {
	return r.rule
}

type RustBinaryBuildStep struct {
	target.CommandBuildStepBase
	target target.Target
	RustBinaryConfig
}

func (a RustBinaryBuildStep) Description() string {
	return fmt.Sprintf("rust binary (cargo) [%s] build %s", a.target.Rule().BuildFor, a.target.Rule().Identifier)
}

func (a RustBinaryBuildStep) rustCFlags() []string {
	return append(
		a.RustCFlags[nativeDefaultFlagsKey],
		append(
			a.RustCFlags[runtime_config.DeviceArchitecture()],
			//fmt.Sprintf("--crate-type=%s", a.OutputType),
		)...,
	)
}

func (a RustBinaryBuildStep) Command() shell.Command {
	if !runtime_config.Config.ProjectConfig().UseRust() {
		fmt.Printf("Note: rust is not enabled but %s is a rust binary target\n", a.target.Rule().Identifier)
	}
	return shell.CommandString(
		fmt.Sprintf(`cd '%s' && RUSTFLAGS="%s" cargo +%s build -v`,
			a.target.Rule().SourceDir(),
			strings.Join(a.rustCFlags(), " "),
			runtime_config.Config.ProjectConfig().RustChannel()),
	)
}

func (a RustBinaryBuildStep) cargoTargetDir() string {
	return path.Join(a.target.Rule().OutputDirFromSourceDir(), "rust_target")
}

func (a RustBinaryBuildStep) Arguments() shell.Arguments {
	rustup.AddTarget(runtime_config.DeviceArchitecture().RustTargetTriple())
	return *shell.ArgumentsFromStrings(
		"-Z", "unstable-options",
		"--target-dir", a.cargoTargetDir(),
		"--out-dir", a.target.Rule().OutputDirFromSourceDir(),
		"--target", runtime_config.DeviceArchitecture().RustTargetTriple(),
	).AppendStrings(a.CargoFlags...)
}

func (a RustBinaryBuildStep) InputFiles() []string {
	files := []string{"Cargo.toml", "Cargo.lock"}
	files = append(files, fs.FindFiles(
		a.target.Rule().SourceDir(),
		rustBinarySourceFileRegex,
	)...)
	files = append(files, fs.FindFilesExact(a.target.Rule().SourceDir(), "config.toml")...)
	return files
}

func (r *RustBinaryTarget) UseRule(rule *rule.Rule) {
	r.rule = rule
}

func (r RustBinaryTarget) BuildSteps() target.BuildSteps {
	if !r.isComplete {
		r.isComplete = true
	}
	rustOutputFile := strings.ReplaceAll(r.Rule().Identifier, ".", "_")
	if r.OutputType == "rlib" {
		rustOutputFile = fmt.Sprintf("lib%s.rlib", rustOutputFile)
	} else if r.OutputType == "staticlib" {
		rustOutputFile = fmt.Sprintf("lib%s.a", rustOutputFile)
	}

	buildStep := RustBinaryBuildStep{
		target:           &r,
		RustBinaryConfig: r.RustBinaryConfig,
	}
	steps := target.BuildSteps{
		rustOutputFile: buildStep,
	}

	return steps
}

func (r RustBinaryTarget) BuildRuleNameExtras() string {
	return ""
}

func (r *RustBinaryTarget) ParseConfigIfNecessary() {
	if err := target.DecodeConfig(r, r.rule); err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("could not decode config %v", r.rule.Config)))
	}
}

var _ target.Target = (*RustBinaryTarget)(nil)
var _ target.CommandBuildStep = (*RustBinaryBuildStep)(nil)

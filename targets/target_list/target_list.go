package target_list

import (
	"fmt"
	"gonani/defs"
	"gonani/rule"
	"gonani/target"
	"gonani/targets"
)

func NewTarget(t defs.TargetType) target.Target {
	switch t {
	case defs.GoBinary:
		return &targets.GoBinaryTarget{}
	case defs.AsmObject:
		return &targets.AsmObjectTarget{}
	case defs.NativeBinary:
		return &targets.NativeBinaryTarget{}
	case defs.RustBinary:
		return &targets.RustBinaryTarget{}
	case defs.Make:
		return &targets.MakeTarget{}
	case defs.Touch:
		return &targets.TouchTarget{}
	case defs.CopyHeaders:
		return &targets.CopyHeadersTarget{}
	default:
		panic(fmt.Sprintf("Unknown type %s", t))
	}
}

func TargetForRule(r *rule.Rule, w target.BuildRuleWriter) target.Target {
	t := NewTarget(r.Type)
	target.Initialize(t, r, w)
	return t
}

package targets

import (
	"fmt"
	"gonani/collections"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
	"reflect"

	"github.com/pkg/errors"
)

type TargetName = string

type MakeBuildIn string

const (
	MakeBuildInOut     MakeBuildIn = "out"
	MakeBuildInSrc     MakeBuildIn = "src"
	MakeBuildInDefault             = MakeBuildInSrc
)

type MakeTargetOptions struct {
	Output         target.OutputFile
	Inputs         []string
	Dependencies   []defs.RuleIdentifier
	GitHeadRebuild bool `yaml:"git_head_rebuild"`
}

type MakeConfig struct {
	Configure      string
	Targets        map[TargetName]interface{}
	InstallTargets map[TargetName]interface{} `yaml:"install_targets"`
	BuildIn        MakeBuildIn                `yaml:"build_in"`
	Jobs           int                        `yaml:"jobs"`
	MkDirs         []string                   `yaml:"mkdirs"`
	Vars           map[string]string          `yaml:"vars"`
	ToolchainId    defs.RuleIdentifier        `yaml:"cross_toolchain"`
	NativeExportHeadersConfig

	m *MakeTarget
}

type MakeTarget struct {
	target.Base
	target.UsingToolchainExtension
	MakeConfig            MakeConfig `yaml:"make"`
	hasConfigureSubTarget bool

	rule       *rule.Rule
	subTargets []target.Target
}

type MakeConfigureSubTarget struct {
	MakeTarget

	parentDependencies []defs.RuleIdentifier
}

type MakeConfigureStep struct {
	target.CommandBuildStepBase
	m *MakeTarget
}

func (m MakeConfigureStep) Description() string {
	return fmt.Sprintf("configure [make] %s", m.m.Rule().Identifier)
}

func (m MakeConfigureStep) Command() shell.Command {
	buildDir := m.m.rule.SourceDir()
	if m.m.MakeConfig.BuildIn == MakeBuildInOut {
		buildDir = m.m.rule.OutputDirFromProjectDir()
	}
	return shell.Command{
		Format: "( cd %s && %s/$in %s ) && touch $out",
		Args: []interface{}{
			buildDir, m.m.rule.RelativeProjectDirFromOutputDir(), m.m.MakeConfig.FilledConfigure(),
		},
	}
}

func (m MakeConfigureStep) Arguments() shell.Arguments {
	return shell.Arguments{}
}

func (m MakeConfigureStep) InputFiles() []string {
	return []string{
		"configure",
	}
}

type MakeTargetStep struct {
	target.CommandBuildStepBase
	m *MakeTarget

	TargetName string
	Inputs     []string
}

func (m MakeTargetStep) Description() string {
	return fmt.Sprintf("make %s/%s", m.m.Rule().Identifier, m.TargetName)
}

func (m MakeTargetStep) Command() shell.Command {
	return shell.CommandString("")
}

func (m MakeTargetStep) Arguments() shell.Arguments {
	buildDir := m.m.rule.SourceDir()
	if m.m.MakeConfig.BuildIn == MakeBuildInOut {
		buildDir = m.m.rule.OutputDirFromProjectDir()
	}
	return shell.Arguments{
		shell.Argument{
			Format: "( cd %s && make %s -j%d %s ) && touch $out",
			Args: []interface{}{
				buildDir,
				target.VarString(m.m.MakeConfig.Vars, m.m.Rule()),
				m.m.MakeConfig.Jobs,
				m.TargetName,
			},
		},
	}
}

func (m MakeTargetStep) InputFiles() []string {
	return m.Inputs
}

type MakeInstallStep struct {
	target.CommandBuildStepBase
	m *MakeTarget

	TargetName string
	Inputs     []string
}

func (m MakeInstallStep) Description() string {
	return fmt.Sprintf("make install %s/%s", m.m.Rule().Identifier, m.TargetName)
}

func (m MakeInstallStep) Command() shell.Command {
	return shell.CommandString("")
}

func (m MakeInstallStep) Arguments() shell.Arguments {
	buildDir := m.m.rule.SourceDir()
	if m.m.MakeConfig.BuildIn == MakeBuildInOut {
		buildDir = m.m.rule.OutputDirFromProjectDir()
	}
	return shell.Arguments{
		shell.Argument{
			Format: "( cd %s && make %s %s ) && touch $out",
			Args: []interface{}{
				buildDir,
				target.VarString(m.m.MakeConfig.Vars, m.m.Rule()),
				m.TargetName,
			},
		},
	}
}

func (m MakeInstallStep) InputFiles() []string {
	return m.Inputs
}

type MakeTargetSubTarget struct {
	target.SimpleTargetBase
	makeTarget MakeTarget

	targetName TargetName
	MakeTargetOptions
	parentDependencies []defs.RuleIdentifier
	rule               *rule.Rule
}

func (m *MakeTargetSubTarget) SubTargets() []target.Target {
	return nil
}

type MakeInstallSubTarget struct {
	target.SimpleTargetBase
	makeTarget MakeTarget

	MakeTargetOptions
	targetName         string
	parentDependencies []defs.RuleIdentifier
	rule               *rule.Rule
}

func (m *MakeInstallSubTarget) SubTargets() []target.Target {
	return nil
}

func (m *MakeConfigureSubTarget) Rule() *rule.Rule {
	r := *m.rule
	r.SharedIdentifier = r.Identifier
	r.Identifier += "__configure"
	r.DependsOn = m.parentDependencies
	return &r
}

func (m *MakeConfigureSubTarget) BuildSteps() target.BuildSteps {
	return target.BuildSteps{
		".configured": MakeConfigureStep{
			m: &m.MakeTarget,
		},
	}
}

func (m *MakeConfigureSubTarget) SubTargets() []target.Target {
	return nil
}

func (m *MakeTargetSubTarget) Rule() *rule.Rule {
	if m.rule == nil {
		r := *m.makeTarget.rule
		r.SharedIdentifier = r.Identifier
		r.Identifier = m.MyIdentifier()
		r.DependsOn = m.parentDependencies
		if m.makeTarget.hasConfigureSubTarget {
			r.DependsOn = append(r.DependsOn, r.SharedIdentifier+"__configure")
		}
		for _, subTargetDependency := range m.Dependencies {
			if subTargetDependency == r.Identifier || subTargetDependency == r.SharedIdentifier {
				continue
			}
			r.DependsOn = append(r.DependsOn, m.makeTarget.MakeSubTargetIdentifier(subTargetDependency))
		}
		m.rule = &r
	}
	return m.rule
}

func (m *MakeTargetSubTarget) MyIdentifier() defs.RuleIdentifier {
	return m.makeTarget.MakeSubTargetIdentifier(m.targetName)
}

func (m *MakeTarget) MakeSubTargetIdentifier(targetName string) defs.RuleIdentifier {
	return m.rule.Identifier + "__target_" + targetName
}

func (m *MakeInstallSubTarget) MyIdentifier() defs.RuleIdentifier {
	return m.makeTarget.MakeSubInstallTargetIdentifier(m.targetName)
}

func (m *MakeTarget) MakeSubInstallTargetIdentifier(targetName string) defs.RuleIdentifier {
	return m.rule.Identifier + "__install_" + targetName
}

func (m *MakeTargetSubTarget) BuildSteps() target.BuildSteps {
	buildSteps := target.BuildSteps{}
	if len(m.Output) == 0 {
		m.Output = "make_target_" + m.targetName
	}
	buildSteps[m.Output] = MakeTargetStep{
		m:          &m.makeTarget,
		TargetName: m.targetName,
		Inputs:     m.Inputs,
	}
	return buildSteps
}

func (m *MakeInstallSubTarget) Rule() *rule.Rule {
	if m.rule == nil {
		r := *m.makeTarget.rule
		r.SharedIdentifier = r.Identifier
		r.Identifier = m.MyIdentifier()
		r.DependsOn = m.parentDependencies
		collections.IterSorted(m.makeTarget.MakeConfig.TargetsWithOptions(), func(makeTargetName interface{}, _ interface{}) {
			r.DependsOn = append(r.DependsOn, m.makeTarget.MakeSubTargetIdentifier(makeTargetName.(string)))
		})
		for _, subTargetDependency := range m.Dependencies {
			if subTargetDependency == r.Identifier || subTargetDependency == r.SharedIdentifier {
				continue
			}
			r.DependsOn = append(r.DependsOn, m.makeTarget.MakeSubInstallTargetIdentifier(subTargetDependency))
		}
		m.rule = &r
	}
	return m.rule
}

func (m *MakeInstallSubTarget) BuildSteps() target.BuildSteps {
	buildSteps := target.BuildSteps{}
	if len(m.Output) == 0 {
		m.Output = "make_install_" + m.targetName
	}
	buildSteps[m.Output] = MakeInstallStep{
		m:          &m.makeTarget,
		TargetName: m.targetName,
		Inputs:     m.Inputs,
	}
	return buildSteps
}

func (m *MakeTarget) SubTargets() []target.Target {
	if m.subTargets == nil {
		var targets []target.Target
		if fs.FileExists(m.Rule().RelativeSourceFileAbsolute("configure")) {
			targets = append(targets, &MakeConfigureSubTarget{
				MakeTarget:         *m,
				parentDependencies: m.rule.DependsOn,
			})
			m.hasConfigureSubTarget = true
		}
		targetsWithOptions := m.MakeConfig.TargetsWithOptions()
		collections.IterSorted(targetsWithOptions, func(targetName interface{}, targetOptions interface{}) {
			targets = append(targets,
				&MakeTargetSubTarget{
					makeTarget:         *m,
					targetName:         targetName.(string),
					MakeTargetOptions:  targetOptions.(MakeTargetOptions),
					parentDependencies: m.rule.DependsOn,
				},
			)
		})

		installTargetsWithOptions := m.MakeConfig.InstallTargetsWithOptions()
		collections.IterSorted(installTargetsWithOptions, func(targetName interface{}, targetOptions interface{}) {
			targets = append(targets, &MakeInstallSubTarget{
				makeTarget:         *m,
				targetName:         targetName.(string),
				MakeTargetOptions:  targetOptions.(MakeTargetOptions),
				parentDependencies: m.rule.DependsOn,
			})
		})
		m.subTargets = targets

		for _, subTarget := range targets {
			m.rule.DependsOn = append(m.rule.DependsOn, subTarget.Rule().Identifier)
		}

		if len(m.MakeConfig.ExportHeaderDirs) > 0 {
			copyHeadersTarget := &CopyHeadersTarget{
				NativeExportHeadersConfig: m.MakeConfig.NativeExportHeadersConfig,
				ruleId:                    m.Rule().Identifier,
			}
			target.Initialize(copyHeadersTarget, CopyHeadersGenerateRule(m), m.BuildRuleWriter)
			m.BuildRuleWriter.WriteTargetBuildRules(copyHeadersTarget)
		}
	}
	return m.subTargets
}

func (m *MakeConfig) FilledConfigure() string {
	return m.m.rule.ProcessTemplate(m.Configure)
}

type MakeBuildTarget struct {
	target.CommandBuildStepBase
	TargetName string
	OutputFile target.OutputFile
	m          *MakeTarget
}

func (m MakeBuildTarget) Description() string {
	return fmt.Sprintf("make %s", m.TargetName)
}

func (m MakeBuildTarget) Command() shell.Command {
	return shell.CommandString("")
}

func (m MakeBuildTarget) Arguments() shell.Arguments {
	return nil
}

func (m MakeBuildTarget) InputFiles() []string {
	return nil
}

func (m *MakeTarget) UseRule(rule *rule.Rule) {
	m.rule = rule
}

func (m *MakeTarget) BuildSteps() target.BuildSteps {
	buildSteps := target.BuildSteps{}
	for _, dirToMake := range m.MakeConfig.MkDirs {
		buildSteps[m.rule.ProcessTemplate(dirToMake)] = MkdirStep{}
	}
	return buildSteps
}

func (m MakeTarget) BuildRuleNameExtras() string {
	return ""
}

func (m MakeTarget) RebuildStrategy() target.RebuildStrategy {
	return target.RebuildNever
}

func processMakeTargetOptions(targetDefinitions map[TargetName]interface{}) map[TargetName]MakeTargetOptions {
	targets := map[TargetName]MakeTargetOptions{}
	collections.IterSorted(targetDefinitions, func(targetName interface{}, rawTarget interface{}) {
		var options MakeTargetOptions
		switch rawTarget.(type) {
		case string:
			options = MakeTargetOptions{
				Output:       rawTarget.(string),
				Dependencies: nil,
			}
		case map[string]interface{}:
			err := defs.ConfigDecoder(&options).Decode(rawTarget)
			if err != nil {
				panic(err)
			}
		case nil:
			options = MakeTargetOptions{
				Output:       "",
				Dependencies: nil,
			}
		default:
			fmt.Printf(
				"Target \"%s\" with options type is %v which is incompatible\n",
				targetName, reflect.TypeOf(rawTarget),
			)
			panic("type is incompatible")
		}
		if options.GitHeadRebuild {
			options.Inputs = append(options.Inputs, ".git/HEAD")
		}
		targets[targetName.(string)] = options

	})
	return targets
}

func (m *MakeConfig) TargetsWithOptions() map[TargetName]MakeTargetOptions {
	return processMakeTargetOptions(m.Targets)
}

func (m *MakeConfig) InstallTargetsWithOptions() map[TargetName]MakeTargetOptions {
	return processMakeTargetOptions(m.InstallTargets)
}

func (m *MakeTarget) ParseConfigIfNecessary() {
	if err := target.DecodeConfig(m, m.rule); err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("could not decode config %v", m.rule.Config)))
	}
	m.MakeConfig.m = m
	if len(m.MakeConfig.BuildIn) == 0 {
		m.MakeConfig.BuildIn = MakeBuildInDefault
	}
	if m.MakeConfig.Jobs == 0 {
		m.MakeConfig.Jobs = 2
	}
	if m.Rule().BuildFor.NeedsCrossCompiler() {
		m.CrossToolchainId[runtime_config.DeviceArchitecture()] = m.MakeConfig.ToolchainId
		if len(m.CrossToolchainId[runtime_config.DeviceArchitecture()]) > 0 {
			if _, exists := m.MakeConfig.Vars["CC"]; !exists {
				m.MakeConfig.Vars["CC"] = m.CCompilerPath()
			}
			if _, exists := m.MakeConfig.Vars["CXX"]; !exists {
				m.MakeConfig.Vars["CXX"] = m.CXXCompilerPath()
			}
			if _, exists := m.MakeConfig.Vars["AS"]; !exists {
				m.MakeConfig.Vars["AS"] = m.AssemblerPath()
			}
			if _, exists := m.MakeConfig.Vars["LD"]; !exists {
				m.MakeConfig.Vars["LD"] = m.LinkerPath()
			}
		}
	}
}

func (m *MakeTarget) Rule() *rule.Rule {
	return m.rule
}

var _ target.Target = (*MakeTarget)(nil)
var _ target.Target = (*MakeConfigureSubTarget)(nil)
var _ target.Target = (*MakeTargetSubTarget)(nil)
var _ target.Target = (*MakeInstallSubTarget)(nil)
var _ target.CommandBuildStep = (*MakeBuildTarget)(nil)
var _ target.CommandBuildStep = (*MakeConfigureStep)(nil)
var _ target.CommandBuildStep = (*MakeTargetStep)(nil)
var _ target.CommandBuildStep = (*MakeInstallStep)(nil)

package targets

import (
	"fmt"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
	"gonani/tools"

	"github.com/pkg/errors"
)

const asmObjectFileExt = "o"

type AsmObjectTarget struct {
	target.Base
	target.SrcFilesTargetBase
	target.HeaderFilesTargetBase
	target.UsingToolchainExtension
	AsmObjectConfig

	rule       *rule.Rule
	isComplete bool
}

type AsmObjectConfig struct {
	ASFlags        map[defs.Architecture][]string   `yaml:"asflags"`
	UseHeadersFrom map[defs.RuleIdentifier][]string `yaml:"use_headers_from"`
	UseHeaders     map[defs.RuleIdentifier][]string `yaml:"use_headers"`
	OverrideTarget map[defs.Architecture]string     `yaml:"override_target"`
}

func (a AsmObjectConfig) ArchASFlags() []string {
	if flags, exist := a.ASFlags[runtime_config.DeviceArchitecture()]; exist {
		return flags
	}
	return a.ASFlags[nativeDefaultFlagsKey]
}

func (a *AsmObjectTarget) SubTargets() []target.Target {
	return nil
}

func (a *AsmObjectTarget) RebuildStrategy() target.RebuildStrategy {
	return target.RebuildLazy
}

func (a *AsmObjectTarget) Rule() *rule.Rule {
	return a.rule
}

type AsmObjectBuildStep struct {
	target.CommandBuildStepBase
	CTExt  target.UsingToolchainExtension
	target target.Target
	AsmObjectConfig
	srcFile string
	headers []string
}

func (a AsmObjectBuildStep) Description() string {
	return fmt.Sprintf("AS [%s] %s", a.target.Rule().BuildFor, a.srcFile)
}

func (a AsmObjectBuildStep) Command() shell.Command {
	return shell.CommandString(a.CTExt.AssemblerPath())
}

func (a AsmObjectBuildStep) usingHeaders() map[defs.RuleIdentifier][]string {
	merged := map[defs.RuleIdentifier][]string{}
	for key, value := range a.UseHeadersFrom {
		merged[key] = value
	}
	for key, value := range a.UseHeaders {
		merged[key] = value
	}
	return merged
}

func (a AsmObjectBuildStep) Arguments() shell.Arguments {
	return a.CTExt.Toolchain().Toolchain.Assembler().Arguments(
		a.target.Rule().ProcessTemplates(a.ArchASFlags()), a.usingHeaders(),
		tools.NativeToolchainOptions{
			OverrideTarget: a.OverrideTarget[runtime_config.DeviceArchitecture()],
			PrioritizedHeaderSearchPaths: []string{
				a.target.Rule().SourceDirRelativeToProjectDir(),
			},
			SourceFiles: []string{a.target.Rule().FileInSourceDirRelativeToProjectDir(a.srcFile)},
			BuildFor:    a.target.Rule().BuildFor,
		},
	)
}

func (a AsmObjectBuildStep) InputFiles() []string {
	return append(a.headers, a.srcFile)
}

func (a *AsmObjectTarget) UseRule(rule *rule.Rule) {
	a.rule = rule
}

func (a AsmObjectTarget) BuildSteps() target.BuildSteps {
	if !a.isComplete {
		for headerDirFrom := range a.UseHeadersFrom {
			a.rule.DependsOn = append(a.rule.DependsOn, CopyHeadersRuleIdForId(headerDirFrom))
		}
		for headerRule := range a.UseHeaders {
			a.rule.DependsOn = append(a.rule.DependsOn, headerRule)
		}
		a.isComplete = true
	}

	steps := target.BuildSteps{}

	files := a.AllSrcFiles(a.rule)
	for _, file := range files {
		steps[fs.ReplaceExtension(file, asmObjectFileExt)] = AsmObjectBuildStep{
			target:          &a,
			CTExt:           a.UsingToolchainExtension,
			AsmObjectConfig: a.AsmObjectConfig,
			srcFile:         file,
			headers:         a.HeaderFiles,
		}
	}

	return steps
}

func (a AsmObjectTarget) BuildRuleNameExtras() string {
	return ""
}

func (a *AsmObjectTarget) ParseConfigIfNecessary() {
	if err := target.DecodeConfig(a, a.rule); err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("could not decode config %v", a.rule.Config)))
	}
	a.HeaderFilesTargetBase.FillDefaults()
	for arch, flags := range a.ASFlags {
		a.ASFlags[arch.Normalize()] = flags
	}
	a.UsingToolchainExtension.Rule = a.rule

	if !a.UsingToolchainExtension.Toolchain().IsHost {
		a.rule.MarkCrossCompilable()
	}
}

var _ target.Target = (*AsmObjectTarget)(nil)
var _ target.CommandBuildStep = (*AsmObjectBuildStep)(nil)

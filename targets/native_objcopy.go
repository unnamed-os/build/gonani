package targets

import (
	"fmt"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
	"gonani/tools"
	"strings"
)

type NativeObjCopyConfig struct {
	NativeObjCopyDetails
	ObjectFilename string `yaml:"-"`
	OutputFilename string `yaml:"-"`
}

type NativeObjCopyDetails struct {
	CrossToolchain map[defs.Architecture]defs.RuleIdentifier `yaml:"cross_toolchain"`
	OverrideTarget map[defs.Architecture]string              `yaml:"override_target"`
	Args           string                                    `yaml:"args"`
	OutExtension   string                                    `yaml:"out_ext"`
}

type NativeObjCopyStep struct {
	target.CommandBuildStepBase
	target.Target
	CTExt target.UsingToolchainExtension
	NativeObjCopyConfig
}

type NativeObjCopyTarget struct {
	target.SimpleTargetBase
	NativeObjCopyConfig
	parent target.Target
	rule   *rule.Rule
}

func (n *NativeObjCopyTarget) Rule() *rule.Rule {
	if n.rule == nil {
		if n.parent != nil {
			r := *n.parent.Rule()
			pId := r.Identifier
			r.SharedIdentifier = pId
			r.Identifier = pId + ".__objcopy"
			n.rule = &r
		} else {
			n.rule = n.SimpleTargetBase.Rule()
		}

		n.rule.BuildFrom = n.rule.OutputDirFromProjectDir()
		n.rule.BuildFromIsProjectRelative = true

		if len(n.CrossToolchain[runtime_config.DeviceArchitecture()]) > 0 {
			n.rule.DependsOn = append(n.rule.DependsOn, n.CrossToolchain[runtime_config.DeviceArchitecture()])
		}
	}

	return n.rule
}

func (n NativeObjCopyTarget) BuildSteps() target.BuildSteps {
	return target.BuildSteps{
		n.OutputFilename: &NativeObjCopyStep{
			Target: &n,
			CTExt: target.UsingToolchainExtension{
				Rule: n.Rule(),
			},
			NativeObjCopyConfig: n.NativeObjCopyConfig,
		},
	}

}

func (n NativeObjCopyStep) Description() string {
	return fmt.Sprintf("OBJ [%s] %s", n.Target.Rule().BuildFor, n.Target.Rule().Identifier)
}

func (n NativeObjCopyStep) Command() shell.Command {
	if len(n.CrossToolchain) != 0 && n.CTExt.CrossToolchainId[runtime_config.DeviceArchitecture()] !=
		n.CrossToolchain[runtime_config.DeviceArchitecture()] {
		n.CTExt.CrossToolchainId = n.CrossToolchain
	}
	return shell.CommandString(n.CTExt.ObjCopyPath())
}

func (n NativeObjCopyStep) Arguments() shell.Arguments {
	return n.CTExt.Toolchain().Toolchain.ObjCopy().Arguments(
		strings.Split(n.Args, " "),
		tools.NativeToolchainOptions{
			OverrideTarget: n.OverrideTarget[runtime_config.DeviceArchitecture()],
			BuildFor:       n.Rule().BuildFor,
		},
	)
}

func (n NativeObjCopyStep) InputFiles() []string {
	return []string{n.ObjectFilename}
}

var _ target.CommandBuildStep = (*NativeObjCopyStep)(nil)
var _ target.Target = (*NativeObjCopyTarget)(nil)

package targets

import (
	"fmt"
	"gonani/collections"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"gonani/rule"
	"gonani/target"
	"path"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

const nativeDefaultFlagsKey = "default"
const objectFileExt = "o"

var nativeBinaryCSourceFileRegex = regexp.MustCompile("^(.*)[.](c(c|pp|xx)?|s|S)$")

type NativeExportHeadersConfig struct {
	ExportHeaderDirs []string `yaml:"export_header_dirs"`
}

type NativeBinaryConfig struct {
	CustomCRT      map[defs.Architecture]bool                `yaml:"custom_crt"`
	ArchDirs       []string                                  `yaml:"arch_dirs"`
	UseHeadersFrom map[defs.RuleIdentifier][]string          `yaml:"use_headers_from"`
	UseHeaders     map[defs.RuleIdentifier][]string          `yaml:"use_headers"`
	CopyOutTo      string                                    `yaml:"copy_out_to"`
	CopyOutFrom    map[defs.RuleIdentifier]map[string]string `yaml:"copy_out_from"`
	ExcludeDirs    map[defs.Architecture][]string            `yaml:"exclude_dirs"`
	SrcDirs        map[defs.Architecture][]string            `yaml:"src_dirs"`
	NativeExportHeadersConfig

	target.UsingToolchainExtension
}

type NativeBinaryTarget struct {
	target.Base
	target.SrcDirTargetBase
	target.HeaderFilesTargetBase
	target.OutputFilenameTargetBase
	NativeLinkConfig
	NativeBinaryConfig
	ObjCopyConfig map[defs.Architecture]NativeObjCopyConfig `yaml:"objcopy"`
	CObjectConfig
	AsmObjectConfig
	RustObjectConfig

	rule     *rule.Rule
	linkStep *NativeLinkStep
}

func (n *NativeBinaryTarget) SubTargets() []target.Target {
	var subTargets []target.Target

	if objCopyConfig, isSpecified := n.ObjCopyConfig[runtime_config.DeviceArchitecture()]; isSpecified {
		objCopyConfig.ObjectFilename = n.linkStep.OutputFilename
		objCopyConfig.OutputFilename = fs.ReplaceExtension(objCopyConfig.ObjectFilename, objCopyConfig.OutExtension)
		objCopyTarget := NativeObjCopyTarget{
			NativeObjCopyConfig: objCopyConfig,
			parent:              n,
		}
		subTargets = append(subTargets, &objCopyTarget)
	}

	return subTargets
}

func (n *NativeBinaryTarget) RebuildStrategy() target.RebuildStrategy {
	return target.RebuildLazy
}

func (n *NativeBinaryTarget) Rule() *rule.Rule {
	return n.rule
}

func (n *NativeBinaryTarget) UseRule(rule *rule.Rule) {
	n.rule = rule
}

func (n NativeBinaryTarget) FindMatchingHeaders(file string) []string {
	return fs.FindPaths(n.AbsSrcDir(n.Rule()), regexp.MustCompile(
		regexp.QuoteMeta(fs.RemoveExtension(file))+"[.](h(pp)?)"))
}

const (
	crtiName = "crti.s"
	crtnName = "crtn.s"
)

const crtStateNotWanted = 0
const crtStateNotFound = 1
const crtStateFound = 2

func (n *NativeBinaryTarget) BuildSteps() target.BuildSteps {
	steps := target.BuildSteps{}

	var objectFiles []string
	var files []string
	sourceDirs := n.SrcDirs[runtime_config.DeviceArchitecture()]
	if len(sourceDirs) == 0 {
		sourceDirs = append(sourceDirs, ".")
	}
	crtiState := crtStateNotWanted
	crtnState := crtStateNotWanted
	if n.CustomCRT[runtime_config.DeviceArchitecture()] {
		crtiState = crtStateNotFound
		crtnState = crtStateNotFound
	}

	for _, srcDir := range sourceDirs {
		files = append(files, collections.JoinPathsWithPrefixAll(fs.FindFiles(
			n.Rule().RelativeSourceFileAbsolute(srcDir),
			nativeBinaryCSourceFileRegex,
			crtiName, crtnName,
		), srcDir)...)

		if crtiState == crtStateNotFound {
			crti := fs.FindFilesExact(n.Rule().RelativeSourceFileAbsolute(srcDir), crtiName)

			if len(crti) != 0 {
				crtiState = crtStateFound
			}
			files = append(files, crti...)
		}
		if crtnState == crtStateNotFound {
			crtn := fs.FindFilesExact(n.Rule().RelativeSourceFileAbsolute(srcDir), crtnName)

			if len(crtn) != 0 {
				crtnState = crtStateFound
			}
			files = append(files, crtn...)
		}
	}

	if crtnState == crtStateNotFound {
		panic(crtnName + " is missing!")
	}
	if crtiState == crtStateNotFound {
		panic(crtnName + " is missing!")
	}

	for _, file := range files {
		skipFile := false
		for _, archDir := range n.ArchDirs {
			archDirPrefix := path.Clean(archDir) + "/"
			if strings.HasPrefix(file, archDirPrefix) {
				archOfFile := strings.Split(path.Clean(strings.TrimPrefix(file, archDirPrefix)), "/")[0]
				if defs.Architecture(archOfFile).Normalize() != runtime_config.DeviceArchitecture() {
					// Not our target arch, skip this file
					skipFile = true
					break
				}
			}
		}
		for _, excludedDir := range n.ExcludeDirs[runtime_config.DeviceArchitecture()] {
			if strings.HasPrefix(file, path.Clean(excludedDir)+"/") {
				skipFile = true
				break
			}
		}
		if skipFile {
			continue
		}

		var buildStep target.CommandBuildStep
		switch filepath.Ext(file) {
		case ".c":
			cBuildStep := CObjectBuildStep{
				target:        n,
				CObjectConfig: n.CObjectConfig,
				CTExt:         n.UsingToolchainExtension,
				srcFile:       file,
				headers:       n.FindMatchingHeaders(file),
			}
			cBuildStep.CObjectConfig.step = &cBuildStep
			buildStep = cBuildStep
		case ".cpp", ".cxx", ".cc":
			// TODO: implement
			panic("not implemented")
		case ".s", ".S":
			buildStep = AsmObjectBuildStep{
				target:          n,
				AsmObjectConfig: n.AsmObjectConfig,
				CTExt:           n.UsingToolchainExtension,
				srcFile:         file,
				headers:         n.FindMatchingHeaders(file),
			}
		case ".rs":
			buildStep = RustObjectBuildStep{
				target:           n,
				RustObjectConfig: n.RustObjectConfig,
				srcFile:          file,
			}
		}
		objectFile := fs.ReplaceExtension(file, objectFileExt)
		steps[objectFile] = buildStep
		objectFiles = append(objectFiles, n.Rule().FileInOutDirFromSourceDir(objectFile))
	}

	linkStep := NativeLinkStep{
		Target:           n,
		NativeLinkConfig: n.NativeLinkConfig,
		CTExt:            n.UsingToolchainExtension,
		ObjectFiles:      objectFiles,
	}
	linkStep.Fill()
	steps[linkStep.OutputFilename] = linkStep
	n.linkStep = &linkStep

	if len(n.ExportHeaderDirs) > 0 {
		copyHeadersTarget := &CopyHeadersTarget{
			NativeExportHeadersConfig: n.NativeExportHeadersConfig,
			ruleId:                    n.Rule().Identifier,
		}
		target.Initialize(copyHeadersTarget, CopyHeadersGenerateRule(n), n.BuildRuleWriter)
		n.BuildRuleWriter.WriteTargetBuildRules(copyHeadersTarget)
	}

	if len(n.CopyOutTo) > 0 {
		steps[n.CopyOutTo] = &CopyFileBuildStep{
			target: n,
			CopyFileConfig: CopyFileConfig{
				SrcFile: n.Rule().FileInOutDirFromSourceDir(linkStep.OutputFilename),
			},
		}
	}
	for copyFromRuleId, copyInstr := range n.CopyOutFrom {
		for copyFrom, copyTo := range copyInstr {
			steps[copyTo] = &CopyFileBuildStep{
				target: n,
				CopyFileConfig: CopyFileConfig{
					SrcFile: path.Join(
						rule.RelativeToProjectDir(rule.AbsoluteOutputDirForId(copyFromRuleId, n.Rule().BuildFor)),
						copyFrom,
					),
					SrcIsFromRoot: true,
				},
			}
		}
	}

	return steps
}

func (n NativeBinaryTarget) BuildRuleNameExtras() string {
	return ""
}

func (n *NativeBinaryTarget) ParseConfigIfNecessary() {
	if err := target.DecodeConfig(n, n.rule); err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("could not decode config %v", n.rule.Config)))
	}
	n.SrcDirTargetBase.FillDefaults()
	n.HeaderFilesTargetBase.FillDefaults()
	for arch, flags := range n.CFlags {
		n.CFlags[arch.Normalize()] = flags
	}
	for arch, flags := range n.ASFlags {
		n.ASFlags[arch.Normalize()] = flags
	}
	for arch, flags := range n.LDFlags {
		n.LDFlags[arch.Normalize()] = flags
	}
	for arch, cfg := range n.ObjCopyConfig {
		n.ObjCopyConfig[arch.Normalize()] = cfg
	}
	for arch, customCrt := range n.CustomCRT {
		n.CustomCRT[arch.Normalize()] = customCrt
	}
	for arch, dirFlagEntries := range n.CFlagsExtraDirs {
		n.CFlagsExtraDirs[arch.Normalize()] = dirFlagEntries
	}
	n.UsingToolchainExtension.Rule = n.rule

	n.CObjectConfig.UseHeadersFrom = n.NativeBinaryConfig.UseHeadersFrom
	n.AsmObjectConfig.UseHeadersFrom = n.NativeBinaryConfig.UseHeadersFrom
	n.CObjectConfig.UseHeaders = n.NativeBinaryConfig.UseHeaders
	n.AsmObjectConfig.UseHeaders = n.NativeBinaryConfig.UseHeaders

	for headerDirFrom := range n.NativeBinaryConfig.UseHeadersFrom {
		n.rule.DependsOn = append(n.rule.DependsOn, CopyHeadersRuleIdForId(headerDirFrom))
	}
	for headerRule := range n.NativeBinaryConfig.UseHeaders {
		n.rule.DependsOn = append(n.rule.DependsOn, headerRule)
	}

	if !n.UsingToolchainExtension.Toolchain().IsHost {
		n.rule.MarkCrossCompilable()
	}
}

var _ target.Target = (*NativeBinaryTarget)(nil)

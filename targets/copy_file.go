package targets

import (
	"fmt"
	"gonani/defs"
	"gonani/fs/paths"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
	"path"

	"github.com/pkg/errors"
)

type CopyFileTarget struct {
	target.SimpleTargetBase
	CopyFileConfig
	ruleId defs.RuleIdentifier
}

type CopyFileConfig struct {
	SrcFile        string `yaml:"src"`
	DestFile       string `yaml:"dest"`
	SrcIsFromRoot  bool   `yaml:"src_from_root"`
	DestIsFromRoot bool   `yaml:"dest_from_root"`
}

func (c CopyFileTarget) BuildSteps() target.BuildSteps {
	if c.DestIsFromRoot {
		c.Rule().OverwriteOutDir = "."
	}
	return target.BuildSteps{
		c.DestFile: &CopyFileBuildStep{
			CopyFileConfig: c.CopyFileConfig,
			target:         &c,
		},
	}
}

func (c CopyFileTarget) relevantIdentifier() defs.RuleIdentifier {
	if len(c.Rule().SharedIdentifier) > 0 {
		return c.Rule().SharedIdentifier
	} else {
		return c.Rule().Identifier
	}
}

type CopyFileBuildStep struct {
	target target.Target
	target.CommandBuildStepBase
	CopyFileConfig
}

func (c CopyFileBuildStep) Command() shell.Command {
	return shell.CommandString("cp")
}

func (c CopyFileBuildStep) Arguments() shell.Arguments {
	return *shell.ArgumentsFromStrings("$in", "$out")
}

func (c CopyFileBuildStep) InputFiles() []string {
	if c.SrcIsFromRoot {
		return []string{path.Join(c.target.Rule().RelativeProjectDir(), c.SrcFile)}
	}
	return []string{c.SrcFile}
}

func CopyFileRuleIdFor(t target.Target) defs.RuleIdentifier {
	return CopyFileRuleIdForId(t.Rule().Identifier)
}

func CopyFileRuleIdForId(ruleId defs.RuleIdentifier) defs.RuleIdentifier {
	return ruleId + ".copy-file"
}

func CopyFileGenerateRule(t target.Target) *rule.Rule {
	return &rule.Rule{
		Identifier:       CopyFileRuleIdFor(t),
		Type:             t.Rule().Type,
		BuildFor:         t.Rule().BuildFor,
		RulePath:         t.Rule().RulePath,
		RuleDir:          t.Rule().RuleDir,
		BuildFrom:        t.Rule().BuildFrom,
		SharedIdentifier: t.Rule().SharedIdentifier,
		Description:      "Copy file of rule " + t.Rule().Identifier,
		OverwriteOutDir:  ".",
	}
}

func (c *CopyFileTarget) ParseConfigIfNecessary() {
	if err := target.DecodeConfig(c, c.Rule()); err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("could not decode config %v", c.Rule().Config)))
	}
	c.Rule().OverwriteOutDir = paths.HeaderDirOf(c.relevantIdentifier())
}

var _ target.CommandBuildStep = (*CopyFileBuildStep)(nil)
var _ target.Target = (*CopyFileTarget)(nil)

package targets

import (
	"fmt"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
	"gonani/tools"
	"path"
	"strings"

	"github.com/pkg/errors"
)

type CObjectTarget struct {
	target.Base
	target.SrcFilesTargetBase
	target.HeaderFilesTargetBase
	target.UsingToolchainExtension
	CObjectConfig

	rule       *rule.Rule
	isComplete bool
}

type CObjectConfig struct {
	CFlags          map[defs.Architecture][]string            `yaml:"cflags"`
	CFlagsExtraDirs map[defs.Architecture]map[string][]string `yaml:"cflags_extra_dirs"`
	UseHeadersFrom  map[defs.RuleIdentifier][]string          `yaml:"use_headers_from"`
	UseHeaders      map[defs.RuleIdentifier][]string          `yaml:"use_headers"`
	OverrideTarget  map[defs.Architecture]string              `yaml:"override_target"`

	step *CObjectBuildStep
}

func (c CObjectConfig) ArchCFlags() []string {
	var cflags []string
	if flags, exist := c.CFlags[runtime_config.DeviceArchitecture()]; exist {
		cflags = flags
	} else {
		cflags = c.CFlags[nativeDefaultFlagsKey]
	}

	for dir, extraCFlags := range c.CFlagsExtraDirs[runtime_config.DeviceArchitecture()] {
		if strings.HasPrefix(path.Clean(c.step.srcFile), path.Clean(dir)) {
			cflags = append(cflags, extraCFlags...)
		}
	}

	return cflags
}

func (c *CObjectTarget) SubTargets() []target.Target {
	return nil
}

func (c *CObjectTarget) RebuildStrategy() target.RebuildStrategy {
	return target.RebuildLazy
}

func (c *CObjectTarget) Rule() *rule.Rule {
	return c.rule
}

type CObjectBuildStep struct {
	target.CommandBuildStepBase
	CTExt  target.UsingToolchainExtension
	target target.Target
	CObjectConfig
	srcFile string
	headers []string
}

func (a CObjectBuildStep) Description() string {
	return fmt.Sprintf("CC [%s] %s", a.target.Rule().BuildFor, a.srcFile)
}

func (a CObjectBuildStep) Command() shell.Command {
	return shell.CommandString(a.CTExt.CCompilerPath())
}

func (a CObjectBuildStep) DepFile() string {
	return "$out.d"
}

func (a CObjectBuildStep) UseDeps() bool {
	return true
}

func (a CObjectBuildStep) usingHeaders() map[defs.RuleIdentifier][]string {
	merged := map[defs.RuleIdentifier][]string{}
	for key, value := range a.UseHeadersFrom {
		merged[key] = value
	}
	for key, value := range a.UseHeaders {
		merged[key] = value
	}
	return merged
}

func (a CObjectBuildStep) Arguments() shell.Arguments {
	return a.CTExt.Toolchain().Toolchain.CCompiler().Arguments(
		a.target.Rule().ProcessTemplates(a.ArchCFlags()), a.usingHeaders(),
		tools.NativeToolchainOptions{
			OverrideTarget: a.OverrideTarget[runtime_config.DeviceArchitecture()],
			PrioritizedHeaderSearchPaths: []string{
				a.target.Rule().SourceDirRelativeToProjectDir(),
			},
			SourceFiles: []string{a.target.Rule().FileInSourceDirRelativeToProjectDir(a.srcFile)},
			DepFile:     a.DepFile(),
			BuildFor:    a.target.Rule().BuildFor,
		},
	)
}

func (a CObjectBuildStep) InputFiles() []string {
	return append(a.headers, a.srcFile)
}

func (c *CObjectTarget) UseRule(rule *rule.Rule) {
	c.rule = rule
}

func (c CObjectTarget) BuildSteps() target.BuildSteps {
	if !c.isComplete {
		for headerDirFrom := range c.UseHeadersFrom {
			c.rule.DependsOn = append(c.rule.DependsOn, CopyHeadersRuleIdForId(headerDirFrom))
		}
		for headerRule := range c.UseHeaders {
			c.rule.DependsOn = append(c.rule.DependsOn, headerRule)
		}
		c.isComplete = true
	}

	steps := target.BuildSteps{}

	files := c.AllSrcFiles(c.rule)
	for _, file := range files {
		cBuildStep := CObjectBuildStep{
			target:        &c,
			CTExt:         c.UsingToolchainExtension,
			CObjectConfig: c.CObjectConfig,
			srcFile:       file,
			headers:       c.HeaderFiles,
		}
		cBuildStep.CObjectConfig.step = &cBuildStep
		steps[fs.ReplaceExtension(file, objectFileExt)] = cBuildStep
	}

	return steps
}

func (c CObjectTarget) BuildRuleNameExtras() string {
	return ""
}

func (c *CObjectTarget) ParseConfigIfNecessary() {
	if err := target.DecodeConfig(c, c.rule); err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("could not decode config %v", c.rule.Config)))
	}
	c.HeaderFilesTargetBase.FillDefaults()
	for arch, flags := range c.CFlags {
		c.CFlags[arch.Normalize()] = flags
	}
	for arch, dirFlagEntries := range c.CFlagsExtraDirs {
		c.CFlagsExtraDirs[arch.Normalize()] = dirFlagEntries
	}
	c.UsingToolchainExtension.Rule = c.rule

	if !c.UsingToolchainExtension.Toolchain().IsHost {
		c.rule.MarkCrossCompilable()
	}
}

var _ target.Target = (*CObjectTarget)(nil)
var _ target.CommandBuildStep = (*CObjectBuildStep)(nil)

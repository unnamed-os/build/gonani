package targets

import (
	"gonani/defs"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
)

type TouchTarget struct {
	target.SimpleTargetBase

	File string
}

func (t TouchTarget) BuildSteps() target.BuildSteps {
	return target.BuildSteps{
		t.File: TouchStep{},
	}
}

func (t *TouchTarget) GenerateRule(parent *rule.Rule, subName string) *rule.Rule {
	r := &rule.Rule{
		Identifier: parent.Identifier + "__sub-touch_" + subName,
		Type:       "touch",
		BuildFor:   parent.BuildFor,
		Config:     nil,
		DependsOn:  []defs.RuleIdentifier{parent.Identifier},
		RulePath:   parent.RulePath,
		BuildFrom:  parent.BuildFrom,
	}
	r.SetRuleDir(parent.RuleDirOnly())
	return r
}

type TouchStep struct {
	target.CommandBuildStepBase
}

func (t TouchStep) Description() string {
	return "touch $out"
}

func (t TouchStep) Command() shell.Command {
	return shell.CommandString("touch")
}

func (t TouchStep) Arguments() shell.Arguments {
	return *shell.ArgumentsFromStrings("$out")
}

func (t TouchStep) InputFiles() []string {
	return nil
}

var _ target.Target = (*TouchTarget)(nil)
var _ target.CommandBuildStep = (*TouchStep)(nil)

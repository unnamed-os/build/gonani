package targets

import (
	"fmt"
	"gonani/config/runtime_config"
	"gonani/fs"
	"gonani/fs/paths"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
	"path"
	"regexp"

	"github.com/pkg/errors"
)

type GoBinaryTarget struct {
	target.CommandBuildStepBase
	target.Base
	target.SrcDirTargetBase

	rule *rule.Rule
}

func (g GoBinaryTarget) SubTargets() []target.Target {
	return nil
}

func (g GoBinaryTarget) RebuildStrategy() target.RebuildStrategy {
	return target.RebuildLazy
}

func (g GoBinaryTarget) Rule() *rule.Rule {
	return g.rule
}

func (g GoBinaryTarget) Description() string {
	return fmt.Sprintf("go [%s] %s", g.Rule().BuildFor, g.ProjectRelSrcDir(g.Rule()))
}

var goFileRegex = regexp.MustCompile("^.+[.]go$")

func (g GoBinaryTarget) InputFiles() []string {
	return fs.FindFiles(g.rule.SourceDir(), goFileRegex)
}

func (g *GoBinaryTarget) UseRule(rule *rule.Rule) {
	g.rule = rule
}

func (g GoBinaryTarget) BuildRuleNameExtras() string {
	return ""
}

func (g GoBinaryTarget) Command() shell.Command {
	return shell.Command{
		Format: "%s -c",
		Args: []interface{}{
			runtime_config.Env().ShellCommand(),
		},
	}
}

func (g GoBinaryTarget) Arguments() shell.Arguments {
	return shell.Arguments{
		shell.Argument{
			Format: "'cd %[1]s && %[3]s build -o %[2]s/$out' .",
			Args: []interface{}{
				path.Join(g.rule.SourceDirRelativeToProjectDir(), g.SrcDir),
				paths.ProjectDirRelativeTo(g.rule.SourceDir()),
				runtime_config.Env().GoCommand(),
			},
		},
	}
}

func (g GoBinaryTarget) BuildSteps() target.BuildSteps {
	return target.BuildSteps{
		"main": g,
	}
}

func (g *GoBinaryTarget) ParseConfigIfNecessary() {
	if err := target.DecodeConfig(g, g.rule); err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("could not decode config %v", g.rule.Config)))
	}
}

var _ target.Target = (*GoBinaryTarget)(nil)
var _ target.CommandBuildStep = (*GoBinaryTarget)(nil)

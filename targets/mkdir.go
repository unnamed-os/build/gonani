package targets

import (
	"gonani/defs"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
)

type MkdirTarget struct {
	target.SimpleTargetBase

	Dir string
}

func (t MkdirTarget) BuildSteps() target.BuildSteps {
	return target.BuildSteps{
		t.Rule().ProcessTemplate(t.Dir): MkdirStep{},
	}
}

func (t *MkdirTarget) GenerateRule(parent *rule.Rule, subName string) *rule.Rule {
	r := &rule.Rule{
		Identifier: parent.Identifier + "__sub-mkdir_" + subName,
		Type:       "mkdir",
		BuildFor:   parent.BuildFor,
		Config:     nil,
		DependsOn:  []defs.RuleIdentifier{parent.Identifier},
		RulePath:   parent.RulePath,
		BuildFrom:  parent.BuildFrom,
	}
	r.SetRuleDir(parent.RuleDirOnly())
	return r
}

type MkdirStep struct {
	target.CommandBuildStepBase
}

func (t MkdirStep) Description() string {
	return "mkdir $out"
}

func (t MkdirStep) Command() shell.Command {
	return shell.CommandString("mkdir")
}

func (t MkdirStep) Arguments() shell.Arguments {
	return *shell.ArgumentsFromStrings("-p", "$out")
}

func (t MkdirStep) InputFiles() []string {
	return nil
}

var _ target.Target = (*MkdirTarget)(nil)
var _ target.CommandBuildStep = (*MkdirStep)(nil)

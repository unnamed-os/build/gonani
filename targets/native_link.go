package targets

import (
	"fmt"
	"gonani/collections"
	"gonani/config"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"gonani/shell"
	"gonani/target"
	"gonani/target/target_utils"
	"gonani/tools"
	"path"
	"strings"

	"github.com/pkg/errors"
)

type NativeLinkTarget struct {
	target.SrcFilesTargetBase
	target.SimpleTargetBase
	CTExt target.UsingToolchainExtension
	NativeLinkConfig
}

type NativeLinkConfig struct {
	target.OutputFilenameTargetBase
	LinkerFile            map[defs.Architecture]string                `yaml:"linker_file"`
	LinkType              defs.NativeObjectType                       `yaml:"link_type"`
	LinkTypeArch          map[defs.Architecture]defs.NativeObjectType `yaml:"link_type_arch"`
	LinkAgainst           map[defs.RuleIdentifier]LinkAgainstConfig   `yaml:"link"`
	LDFlags               map[defs.Architecture][]string              `yaml:"ldflags"`
	OverrideTarget        map[defs.Architecture]string                `yaml:"override_target"`
	UseCCAsLinker         map[defs.Architecture]bool                  `yaml:"use_cc_as_linker"`
	LinkOverrideExtension map[defs.Architecture]string                `yaml:"link_override_extension"`
}

type LinkAgainstConfig struct {
	StaticLibs     []string `yaml:"static_libs"`
	StaticLibsFind []string `yaml:"static_libs_find"`
	RLibs          []string `yaml:"rlibs"`
	Objs           []string `yaml:"objs"`
	Arch           []defs.Architecture
}

type NativeLinkTemplateBits struct {
	Arch defs.Architecture
}

func (n NativeLinkConfig) ArchLDFlags() []string {
	if flags, exist := n.LDFlags[runtime_config.DeviceArchitecture()]; exist {
		return flags
	}
	return n.LDFlags[nativeDefaultFlagsKey]
}

type NativeLinkStep struct {
	target.CommandBuildStepBase
	target.Target
	CTExt target.UsingToolchainExtension
	NativeLinkConfig
	ObjectFiles []string
}

func (n NativeLinkStep) TemplateBits() NativeLinkTemplateBits {
	return NativeLinkTemplateBits{
		Arch: runtime_config.DeviceArchitecture(),
	}
}

func (n NativeLinkStep) Description() string {
	return fmt.Sprintf("LD [%s] %s", n.Target.Rule().BuildFor, n.Target.Rule().Identifier)
}

func (n *NativeLinkStep) Fill() {
	for arch, flags := range n.LDFlags {
		n.LDFlags[arch.Normalize()] = flags
	}
	for arch, linkerFile := range n.LinkerFile {
		n.LinkerFile[arch.Normalize()] = linkerFile
	}
	for arch, linkType := range n.LinkTypeArch {
		n.LinkTypeArch[arch.Normalize()] = linkType
	}
	if len(n.LinkType) == 0 {
		n.LinkType = defs.NOTypeExecutable
	}
	if len(n.OutputFilename) == 0 {
		if len(n.LinkOverrideExtension[runtime_config.DeviceArchitecture()]) > 0 {
			n.OutputFilename = fs.AddExtension(
				n.Rule().Identifier, n.LinkOverrideExtension[runtime_config.DeviceArchitecture()])
		} else {
			n.OutputFilename = n.FinalLinkType().AddExtTo(n.Rule().Identifier)
		}
	}
}

func (n NativeLinkStep) FinalLinkType() defs.NativeObjectType {
	linkType, isSpecified := n.LinkTypeArch[runtime_config.DeviceArchitecture()]
	if !isSpecified {
		linkType = n.LinkType
	}
	return linkType
}

func (n NativeLinkStep) Command() shell.Command {
	linkerPath := n.CTExt.LinkerPath()
	if useCC, _ := n.UseCCAsLinker[runtime_config.DeviceArchitecture()]; useCC {
		linkerPath = n.CTExt.CCompilerPath()
	}
	return shell.CommandString(linkerPath)
}

func (n NativeLinkStep) Arguments() shell.Arguments {
	ldFlags := n.Rule().ProcessTemplates(n.ArchLDFlags())
	linkType := n.FinalLinkType()
	var searchDirs []string
	var linkLibNames []string
	var linkObjects []string
	var linkerFile string

	for ruleId, linkAgainst := range n.LinkAgainst {
		if len(linkAgainst.Arch) != 0 && !config.IsArchInList(linkAgainst.Arch) {
			continue
		}
		for _, staticLib := range linkAgainst.StaticLibs {
			basename := defs.NOTypeStatic.AddExtTo(target_utils.ProcessTemplate(staticLib, n.TemplateBits()))
			staticLibFile := target.FindFileInOutputOfModuleCommand(n.Target, ruleId, basename, -1)
			searchDirs = append(searchDirs, shell.OutputOfCmdAlt(
				shell.DirNameCommand(shell.OutputOfCmd(staticLibFile))))
			linkLibNames = append(linkLibNames, basename)
		}
		n.Rule().DependsOn = append(n.Rule().DependsOn, ruleId)
	}

	crtiObjSf := "/" + fs.ReplaceExtension(crtiName, asmObjectFileExt)
	crtiObj := collections.FindStringBySuffix(n.ObjectFiles, crtiObjSf)
	if len(crtiObj) > 0 {
		linkObjects = append(linkObjects, path.Join(n.Rule().SourceDirRelativeToProjectDir(), crtiObj))
		n.ObjectFiles = collections.RemoveStringFromSlice(n.ObjectFiles, crtiObj)
		linkObjects = append(linkObjects, fmt.Sprintf("`%s`", n.CRTBeginPathCmdLine()))
	}

	var endArgs []string

	crtnObjSf := "/" + fs.ReplaceExtension(crtnName, asmObjectFileExt)
	crtnObj := collections.FindStringBySuffix(n.ObjectFiles, crtnObjSf)
	if len(crtiObj) > 0 {
		endArgs = append(endArgs, fmt.Sprintf("`%s`", n.CRTEndPathCmdLine()))
		endArgs = append(endArgs, path.Join(n.Rule().SourceDirRelativeToProjectDir(), crtnObj))
		n.ObjectFiles = collections.RemoveStringFromSlice(n.ObjectFiles, crtnObj)
	}

	for ruleId, linkAgainst := range n.LinkAgainst {
		if len(linkAgainst.Arch) != 0 && !config.IsArchInList(linkAgainst.Arch) {
			continue
		}
		for _, obj := range linkAgainst.Objs {
			basename := target_utils.ProcessTemplate(obj, n.TemplateBits())
			if len(path.Ext(basename)) == 0 {
				basename = defs.NOTypeSingle.AddExtTo(basename)
			}
			objFile := target.FindFileInOutputOfModuleCommand(n.Target, ruleId, basename, -1)
			linkObjects = append(linkObjects, shell.OutputOfCmd(objFile))
		}

		for _, rLib := range linkAgainst.RLibs {
			basename := defs.NOTypeRLib.AddExtTo(target_utils.ProcessTemplate(rLib, n.TemplateBits()))
			rLibFile := target.FindFileInOutputOfModuleCommand(n.Target, ruleId, basename, 1)
			linkObjects = append(linkObjects, shell.OutputOfCmd(rLibFile))
		}

		for _, staticLibGlob := range linkAgainst.StaticLibsFind {
			basename := target_utils.ProcessTemplate(staticLibGlob, n.TemplateBits())
			staticLibGlobFile := target.FindFileInOutputOfModuleCommand(n.Target, ruleId, basename, -1)
			linkObjects = append(linkObjects, shell.OutputOfCmd(staticLibGlobFile))
		}
	}
	linkObjects = append(linkObjects, target.PathAddPrefixAll(n.Rule().SourceDirRelativeToProjectDir(), n.ObjectFiles)...)
	linkObjects = append(linkObjects, endArgs...)

	if specifiedLinkerFile, isSpecified := n.LinkerFile[runtime_config.DeviceArchitecture()]; isSpecified {
		linkerFile = n.Rule().RelativeSourceFileRelativeToProjectDir(specifiedLinkerFile)
	}

	linker := n.CTExt.Toolchain().Toolchain.Linker()
	return linker.Arguments(
		ldFlags, linkType, searchDirs, linkLibNames, linkObjects, linkerFile,
		tools.NativeToolchainOptions{
			OverrideTarget: n.OverrideTarget[runtime_config.DeviceArchitecture()],
			UseCCAsLinker:  n.UseCCAsLinker[runtime_config.DeviceArchitecture()],
			BuildFor:       n.Rule().BuildFor,
		},
	)
}

func (n NativeLinkStep) InputFiles() []string {
	files := n.ObjectFiles
	if linkerFile, isSpecified := n.LinkerFile[runtime_config.DeviceArchitecture()]; isSpecified {
		files = append(files, linkerFile)
	}
	return files
}

func (n NativeLinkTarget) BuildSteps() target.BuildSteps {
	linkStep := NativeLinkStep{
		Target:           &n,
		CTExt:            n.CTExt,
		NativeLinkConfig: n.NativeLinkConfig,
	}
	linkStep.Fill()
	return target.BuildSteps{
		linkStep.OutputFilename: linkStep,
	}
}

func (n NativeLinkStep) CRTBeginPathCmdLine() string {
	return n.linkerCmdLine("-print-file-name=crtbegin.o")
}

func (n NativeLinkStep) CRTEndPathCmdLine() string {
	return n.linkerCmdLine("-print-file-name=crtend.o")
}

func (n NativeLinkStep) linkerCmdLine(additionalArgs ...string) string {
	args := shell.ArgumentsFromStrings(n.Rule().ProcessTemplates(n.ArchLDFlags())...)
	args.AppendStrings(additionalArgs...)
	return strings.Join([]string{n.Command().String(), args.String()}, " ")
}

func (n *NativeLinkTarget) ParseConfigIfNecessary() {
	if err := target.DecodeConfig(n, n.Rule()); err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("could not decode config %v", n.Rule().Config)))
	}
	n.CTExt.Rule = n.Rule()

	if !n.CTExt.Toolchain().IsHost {
		n.Rule().MarkCrossCompilable()
	}
}

var _ target.Target = (*NativeLinkTarget)(nil)
var _ target.CommandBuildStep = (*NativeLinkStep)(nil)

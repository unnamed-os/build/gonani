package targets

import (
	"fmt"
	"gonani/defs"
	"gonani/fs"
	"gonani/fs/paths"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
	"regexp"

	"github.com/pkg/errors"
)

type CopyHeadersTarget struct {
	target.SimpleTargetBase
	NativeExportHeadersConfig

	ruleId defs.RuleIdentifier
}

var headerFileRegex = regexp.MustCompile("^.*[.](h(pp)?)$")

func (c CopyHeadersTarget) BuildSteps() target.BuildSteps {
	steps := target.BuildSteps{}

	headerFiles := fs.FindFiles(c.Rule().SourceDir(), headerFileRegex)

	for _, headerFile := range headerFiles {
		steps[headerFile] = &CopyHeadersBuildStep{
			headerFile: headerFile,
		}
	}

	return steps
}

func (c CopyHeadersTarget) relevantIdentifier() defs.RuleIdentifier {
	if len(c.Rule().SharedIdentifier) > 0 {
		return c.Rule().SharedIdentifier
	} else {
		return c.Rule().Identifier
	}
}

type CopyHeadersBuildStep struct {
	target.CommandBuildStepBase
	headerFile string
}

func (c CopyHeadersBuildStep) Command() shell.Command {
	return shell.CommandString("cp")
}

func (c CopyHeadersBuildStep) Arguments() shell.Arguments {
	return *shell.ArgumentsFromStrings("$in", "$out")
}

func (c CopyHeadersBuildStep) InputFiles() []string {
	return []string{c.headerFile}
}

func CopyHeadersRuleIdFor(t target.Target) defs.RuleIdentifier {
	return CopyHeadersRuleIdForId(t.Rule().Identifier)
}

func CopyHeadersRuleIdForId(ruleId defs.RuleIdentifier) defs.RuleIdentifier {
	return ruleId + ".copy-headers"
}

func CopyHeadersGenerateRule(t target.Target) *rule.Rule {
	return &rule.Rule{
		Identifier:       CopyHeadersRuleIdFor(t),
		Type:             t.Rule().Type,
		BuildFor:         t.Rule().BuildFor,
		RulePath:         t.Rule().RulePath,
		RuleDir:          t.Rule().RuleDir,
		BuildFrom:        t.Rule().BuildFrom,
		SharedIdentifier: t.Rule().SharedIdentifier,
		Description:      "Copy headers of rule " + t.Rule().Identifier,
		OverwriteOutDir:  ".",
	}
}

func (c *CopyHeadersTarget) ParseConfigIfNecessary() {
	if err := target.DecodeConfig(c, c.Rule()); err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("could not decode config %v", c.Rule().Config)))
	}
	c.Rule().OverwriteOutDir = paths.HeaderDirOf(c.relevantIdentifier())
}

var _ target.CommandBuildStep = (*CopyHeadersBuildStep)(nil)
var _ target.Target = (*CopyHeadersTarget)(nil)

package targets

import (
	"fmt"
	"gonani/config/runtime_config"
	"gonani/defs"
	"gonani/fs"
	"gonani/rule"
	"gonani/shell"
	"gonani/target"
	"path"
	"strings"

	"github.com/pkg/errors"
)

const rustObjectFileExt = "o"

type RustObjectTarget struct {
	target.Base
	target.SrcFilesTargetBase
	RustObjectConfig

	rule       *rule.Rule
	isComplete bool
}

type RustObjectConfig struct {
	RustCFlags          map[defs.Architecture][]string            `yaml:"rustc_flags"`
	RustCFlagsExtraDirs map[defs.Architecture]map[string][]string `yaml:"rustc_flags_extra_dirs"`
	OverrideTarget      map[defs.Architecture]string              `yaml:"override_target"`

	step *RustObjectBuildStep
}

func (c RustObjectConfig) ArchCFlags() []string {
	var cflags []string
	if flags, exist := c.RustCFlags[runtime_config.DeviceArchitecture()]; exist {
		cflags = flags
	} else {
		cflags = c.RustCFlags[nativeDefaultFlagsKey]
	}

	for dir, extraCFlags := range c.RustCFlagsExtraDirs[runtime_config.DeviceArchitecture()] {
		if strings.HasPrefix(path.Clean(c.step.srcFile), path.Clean(dir)) {
			cflags = append(cflags, extraCFlags...)
		}
	}

	return cflags
}

func (r *RustObjectTarget) SubTargets() []target.Target {
	return nil
}

func (r *RustObjectTarget) RebuildStrategy() target.RebuildStrategy {
	return target.RebuildLazy
}

func (r *RustObjectTarget) Rule() *rule.Rule {
	return r.rule
}

type RustObjectBuildStep struct {
	target.CommandBuildStepBase
	target  target.Target
	srcFile string
	RustObjectConfig
}

func (a RustObjectBuildStep) Description() string {
	return fmt.Sprintf("rustc obj [%s] %s", a.target.Rule().BuildFor, a.srcFile)
}

func (a RustObjectBuildStep) Command() shell.Command {
	if !runtime_config.Config.ProjectConfig().UseRust() {
		fmt.Printf("Note: rust is not enabled but %s is a rust object target\n", a.target.Rule().Identifier)
	}
	return shell.CommandString(
		fmt.Sprintf("cd '%s' && rustup run %s rustc",
			a.target.Rule().SourceDirRelativeToProjectDir(),
			runtime_config.Config.ProjectConfig().RustChannel()),
	)
}

func (a RustObjectBuildStep) Arguments() shell.Arguments {
	return *shell.ArgumentsFromStrings(
		"--crate-name", strings.ReplaceAll(a.target.Rule().Identifier, ".", "_"),
		"-C", "panic=abort",
		"--target", runtime_config.DeviceArchitecture().RustTargetTriple(),
		"--emit", "obj", "--crate-type", "staticlib",
		fmt.Sprintf("%s/$in", a.target.Rule().RelativeProjectDir()),
		"-o", fmt.Sprintf("%s/$out", a.target.Rule().RelativeProjectDir()),
	).AppendStrings(a.ArchCFlags()...)
}

func (a RustObjectBuildStep) InputFiles() []string {
	return []string{a.srcFile}
}

func (r *RustObjectTarget) UseRule(rule *rule.Rule) {
	r.rule = rule
}

func (r RustObjectTarget) BuildSteps() target.BuildSteps {
	if !r.isComplete {
		r.isComplete = true
	}

	steps := target.BuildSteps{}

	files := r.AllSrcFiles(r.rule)
	for _, file := range files {
		cBuildStep := RustObjectBuildStep{
			target:           &r,
			RustObjectConfig: r.RustObjectConfig,
			srcFile:          file,
		}
		cBuildStep.RustObjectConfig.step = &cBuildStep
		steps[fs.ReplaceExtension(file, rustObjectFileExt)] = cBuildStep
	}

	return steps
}

func (r RustObjectTarget) BuildRuleNameExtras() string {
	return ""
}

func (r *RustObjectTarget) ParseConfigIfNecessary() {
	if err := target.DecodeConfig(r, r.rule); err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("could not decode config %v", r.rule.Config)))
	}
	for arch, flags := range r.RustCFlags {
		r.RustCFlags[arch.Normalize()] = flags
	}
	for arch, dirFlagEntries := range r.RustCFlagsExtraDirs {
		r.RustCFlagsExtraDirs[arch.Normalize()] = dirFlagEntries
	}
}

var _ target.Target = (*RustObjectTarget)(nil)
var _ target.CommandBuildStep = (*RustObjectBuildStep)(nil)
